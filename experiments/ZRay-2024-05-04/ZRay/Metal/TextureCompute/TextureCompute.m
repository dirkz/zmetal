//
//  TextureCompute.m
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#import <Metal/Metal.h>

#import "TextureCompute.h"
#import "TextureComputeSharedTypes.h"

@interface TextureCompute ()

@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLTexture> texture;
@property (nonatomic) id<MTLComputePipelineState> pipelineState;
@property (nonatomic) MTLSize threadgroupSize;
@property (nonatomic) MTLSize threadgroupCount;

@end

@implementation TextureCompute

- (instancetype)initWithDevice:(id<MTLDevice>)device width:(NSUInteger)width height:(NSUInteger)height
{
    self = [super init];
    if (self) {
        _device = device;
        _width = width;
        _height = height;
        [self createTexture];
        [self createPipeline];
        [self calculateThreadGroupSize];
    }
    return self;
}

- (void)computeWithCommandBuffer:(id<MTLCommandBuffer>)commandBuffer {
    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    [computeEncoder setComputePipelineState:self.pipelineState];
    [computeEncoder setTexture:self.texture atIndex:TextureIndexOutput];
    [computeEncoder dispatchThreadgroups:self.threadgroupCount threadsPerThreadgroup:self.threadgroupSize];
    [computeEncoder endEncoding];
}

#pragma mark Creation

- (void)createTexture {
    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];

    // Indicate that each pixel has a blue, green, red, and alpha channel, where each channel is
    // an 8-bit unsigned normalized value (i.e. 0 maps to 0.0 and 255 maps to 1.0)
    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;

    // Set the pixel dimensions of the texture
    textureDescriptor.width = self.width;
    textureDescriptor.height = self.height;

    textureDescriptor.usage = MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;

    // Create the texture from the device by using the descriptor
    self.texture = [_device newTextureWithDescriptor:textureDescriptor];
}

- (void)createPipeline {
    id<MTLLibrary> defaultLibrary = [self.device newDefaultLibrary];
    if (!defaultLibrary) {
        NSLog(@"no default library");
        return;
    }

    id<MTLFunction> computeFn = [defaultLibrary newFunctionWithName:@"textureCompute"];

    NSError *error = nil;
    self.pipelineState = [self.device newComputePipelineStateWithFunction:computeFn error:&error];
    if (!self.pipelineState) {
        if (error) {
            NSLog(@"no pipeline state: %@", error);
        } else {
            NSLog(@"no pipeline state");
        }
        return;
    }
}

- (void)calculateThreadGroupSize {
    // Set the compute kernel's threadgroup size to 16 x 16.
    _threadgroupSize = MTLSizeMake(16, 16, 1);

    // Calculate the number of rows and columns of threadgroups given the size of the
    // input image. Ensure that the grid covers the entire image (or more).
    _threadgroupCount.width = (_texture.width + _threadgroupSize.width - 1) / _threadgroupSize.width;
    _threadgroupCount.height = (_texture.height + _threadgroupSize.height - 1) / _threadgroupSize.height;

    // The image data is 2D, so set depth to 1.
    _threadgroupCount.depth = 1;
}

@end
