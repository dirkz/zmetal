//
//  TextureCompute.metal
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#import "TextureComputeSharedTypes.h"

#include <metal_stdlib>
using namespace metal;

template<class T>
T lerp(const T t1, const T t2, float a) {
    return t1 + a * (t2 - t1);
}

kernel void textureCompute(uint2 gid [[thread_position_in_grid]],
                           texture2d<float, access::write> texture [[texture(TextureIndexOutput)]]) {
    if (gid.x >= texture.get_width() || gid.y >= texture.get_height()) {
        return;
    }
    float3 white{1, 1, 1};
    float3 blue{0.5, 0.7, 1.0};
    float a = (float) gid.x / texture.get_width();
    float4 color = float4(lerp(white, blue, a), 1);
    texture.write(color, gid);
}
