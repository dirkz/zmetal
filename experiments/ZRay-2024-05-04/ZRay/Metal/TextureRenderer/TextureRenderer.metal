//
//  TextureRenderer.metal
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#include <metal_stdlib>
using namespace metal;

#import "TextureRendererSharedTypes.h"

struct RasterizerData
{
    float4 position [[position]];
    float2 textureCoordinates;
};

vertex RasterizerData
vertexShader(uint vertexID [[vertex_id]],
             constant Vertex *vertices [[buffer(BufferIndexVertices)]]) {
    float4 position = float4(vertices[vertexID].position, 1);
    RasterizerData rasterizerData{position, vertices[vertexID].textureCoordinates};
    return rasterizerData;
}

fragment float4 fragmentShader(RasterizerData rasterizerData [[stage_in]],
                               texture2d<float> texture [[texture(TextureIndexComputed)]]) {
    constexpr sampler textureSampler (mag_filter::linear, min_filter::linear);
    const float4 colorSample = texture.sample(textureSampler, rasterizerData.textureCoordinates);
    return colorSample;
}
