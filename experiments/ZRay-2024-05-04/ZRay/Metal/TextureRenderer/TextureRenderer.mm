//
//  TextureRenderer.mm
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#import <vector>

#import "TextureRenderer.h"
#import "TextureRendererSharedTypes.h"
#import "TextureCompute.h"

using namespace std;

@interface TextureRenderer ()

@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLRenderPipelineState> pipelineState;
@property (nonatomic) id<MTLCommandQueue> commandQueue;
@property (nonatomic) id<MTLBuffer> vertexBuffer;
@property (nonatomic) id<MTLBuffer> indexBuffer;
@property (nonatomic) NSUInteger indexCount;
@property (nonatomic) id<MTLCommandBuffer> computeAndDrawCommandBuffer;
@property (nonatomic) TextureCompute *textureCompute;

@end

@implementation TextureRenderer

- (instancetype)initWithDevice:(id<MTLDevice>)device view:(MTKView *)view
{
    self = [super init];
    if (self) {
        _device = device;
        _commandQueue = [_device newCommandQueue];
        [self setupBuffers];
        [self setupPipelineDescriptorWithView:view];
    }
    return self;
}

#pragma mark Helpers

template<class T>
size_t vectorSize(const vector<T>& v) {
    return sizeof(T) * v.size();
}

#pragma mark Setup

- (void)setupBuffers {
    Vertex topLeft{{-1, 1, 0}, {0, 0}};
    Vertex topRight{{1, 1, 0}, {1, 0}};
    Vertex bottomLeft{{-1, -1, 0}, {0, 1}};
    Vertex bottomRight{{1, -1, 0}, {1, 1}};
    vector<Vertex> vertices{topLeft, topRight, bottomLeft, bottomRight};
    vector<uint16_t> indices{0, 1, 2, 2, 1, 3}; // clock-wise

    self.vertexBuffer = [self.device newBufferWithBytes:vertices.data()
                                                 length:vectorSize(vertices)
                                                options:MTLResourceCPUCacheModeDefaultCache];

    self.indexBuffer = [self.device newBufferWithBytes:indices.data()
                                                length:vectorSize(indices)
                                               options:MTLResourceCPUCacheModeDefaultCache];
    self.indexCount = indices.size();
}

- (void)setupPipelineDescriptorWithView:(MTKView *)view {
    id<MTLLibrary> defaultLibrary = [self.device newDefaultLibrary];
    if (!defaultLibrary) {
        NSLog(@"no default library");
        return;
    }

    id<MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vertexShader"];
    if (!vertexFunction) {
        NSLog(@"no vertex function");
        return;
    }

    id<MTLFunction> fragmentFunction = [defaultLibrary newFunctionWithName:@"fragmentShader"];
    if (!fragmentFunction) {
        NSLog(@"no fragment function");
        return;
    }

    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"TextureRenderer Pipeline";
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;


    NSError *error = nil;
    self.pipelineState = [self.device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor
                                                                     error:&error];
    if (!self.pipelineState) {
        if (error) {
            NSLog(@"no pipeline state: %@", error);
        } else {
            NSLog(@"no pipeline state");
        }
    }
}

#pragma mark MTKViewDelegate

- (void)drawInMTKView:(nonnull MTKView *)view {
    MTLRenderPassDescriptor *renderPassDescriptor = view.currentRenderPassDescriptor;
    if (!renderPassDescriptor) {
        return;
    }

    // Try to re-use the command buffer that was created by texture creation.
    id<MTLCommandBuffer> commandBuffer = self.computeAndDrawCommandBuffer;

    if (!commandBuffer) {
        // Fall back to temporary command buffer only for drawing this frame.
        commandBuffer = [self.commandQueue commandBuffer];
    }

    id<MTLRenderCommandEncoder> commandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];

    [commandEncoder setRenderPipelineState:self.pipelineState];
    [commandEncoder setFragmentTexture:self.textureCompute.texture atIndex:TextureIndexComputed];
    [commandEncoder setVertexBuffer:self.vertexBuffer offset:0 atIndex:BufferIndexVertices];
    [commandEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                               indexCount:self.indexCount
                                indexType:MTLIndexTypeUInt16
                              indexBuffer:self.indexBuffer
                        indexBufferOffset:0];

    [commandEncoder endEncoding];

    id<MTLDrawable> drawable = view.currentDrawable;
    [commandBuffer presentDrawable:drawable];
    [commandBuffer commit];

    // If there was still a command buffer from the texture creation, we have used it.
    // Forget about it.
    self.computeAndDrawCommandBuffer = nil;
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
    CGFloat aspectRatio = size.width / size.height;
    if (!self.textureCompute ||
        self.textureCompute.width != size.width ||
        self.textureCompute.height != size.height) {
        // Kick-off texture generation on a command buffer that will be re-used for
        // drawing (once), and then forgotten.
        NSUInteger width = 512;
        NSUInteger height = round(width * 1.0 / aspectRatio);
        self.computeAndDrawCommandBuffer = [self.commandQueue commandBuffer];
        self.textureCompute = [[TextureCompute alloc]
                               initWithDevice:self.device
                               width:width
                               height:height];
        [self.textureCompute computeWithCommandBuffer:self.computeAndDrawCommandBuffer];
    }
}

@end
