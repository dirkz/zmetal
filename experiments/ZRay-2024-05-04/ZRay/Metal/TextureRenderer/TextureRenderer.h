//
//  TextureRenderer.h
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TextureRenderer : NSObject <MTKViewDelegate>

- (instancetype)initWithDevice:(id<MTLDevice>)device view:(MTKView *)view;

@end

NS_ASSUME_NONNULL_END
