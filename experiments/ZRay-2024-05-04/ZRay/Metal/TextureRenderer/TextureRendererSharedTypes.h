//
//  TextureRendererSharedTypes.h
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#ifndef TextureRendererSharedTypes_h
#define TextureRendererSharedTypes_h

#import <simd/simd.h>

using namespace simd;

enum BufferIndex {
    BufferIndexVertices
};

enum TextureIndex {
    TextureIndexComputed
};

struct Vertex {
    float3 position;
    float2 textureCoordinates;
};

#endif /* TextureRendererSharedTypes_h */
