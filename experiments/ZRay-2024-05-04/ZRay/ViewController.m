//
//  ViewController.m
//  ZRay
//
//  Created by Dirk Zimmermann on 4/5/24.
//

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

#import "TextureRenderer.h"

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) id<MTKViewDelegate> renderer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    MTKView *view = (MTKView *) self.view;
    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    self.renderer = [[TextureRenderer alloc] initWithDevice:device view:view];
    view.device = device;
    view.clearColor = MTLClearColorMake(0.0, 0.5, 1.0, 1.0);
    view.delegate = self.renderer;
}


@end
