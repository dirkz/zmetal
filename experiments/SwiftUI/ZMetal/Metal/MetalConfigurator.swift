//
//  MetalConfigurator.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import Foundation
import Metal
import MetalKit

class MetalConfigurator: ObservableObject {
    let defaultPixelFormat: MTLPixelFormat = .bgra8Unorm

    struct Data {
        let device: MTLDevice
        let commandQueue: MTLCommandQueue
        let commandBuffer: MTLCommandBuffer
        let library: MTLLibrary
        let pipelineDescriptor: MTLRenderPipelineDescriptor
        let pixelFormat: MTLPixelFormat
        let renderPipelineState: MTLRenderPipelineState

        func makeRenderCommandEncoder(view: MTKView) -> MTLRenderCommandEncoder {
            guard let renderPassDescriptor = view.currentRenderPassDescriptor else {
                preconditionFailure("no currentRenderPassDescriptor")
            }
            guard let encoder = commandBuffer.makeRenderCommandEncoder(descriptor: renderPassDescriptor) else {
                preconditionFailure("no makeRenderCommandEncoder")
            }

            return encoder
        }
    }

    enum Result {
        case none
        case noDevice
        case noCommandQueue
        case noCommandBuffer
        case noDefaultLibrary
        case noVertexFunction
        case noFragmentFunction
        case couldNotCreateRenderPipelineState
        case success(Data)
    }

    @Published var result: Result = .none

    @Sendable func configure() {
        guard let device = MTLCreateSystemDefaultDevice() else {
            DispatchQueue.main.async {
                self.result = .noDevice
            }
            return
        }
        guard let commandQueue = device.makeCommandQueue() else {
            DispatchQueue.main.async {
                self.result = .noCommandQueue
            }
            return
        }
        guard let commandBuffer = commandQueue.makeCommandBuffer() else {
            DispatchQueue.main.async {
                self.result = .noCommandBuffer
            }
            return
        }
        guard let library = device.makeDefaultLibrary() else {
            DispatchQueue.main.async {
                self.result = .noDefaultLibrary
            }
            return
        }
        guard let vertexFunction = library.makeFunction(name: "vertex_main") else {
            DispatchQueue.main.async {
                self.result = .noVertexFunction
            }
            return
        }
        guard let fragmentFunction = library.makeFunction(name: "fragment_main") else {
            DispatchQueue.main.async {
                self.result = .noFragmentFunction
            }
            return
        }

        let vertexDescriptor = MTLVertexDescriptor()

        let vertices = Int(BUFFER_VERTICES)
        vertexDescriptor.attributes[vertices].format = .float3
        vertexDescriptor.attributes[vertices].bufferIndex = vertices
        vertexDescriptor.layouts[vertices].stride = MemoryLayout<float3>.size

        let pixelFormat = defaultPixelFormat

        let pipelineDescriptor = MTLRenderPipelineDescriptor()
        pipelineDescriptor.vertexFunction = vertexFunction
        pipelineDescriptor.fragmentFunction = fragmentFunction
        pipelineDescriptor.colorAttachments[0].pixelFormat = pixelFormat
        pipelineDescriptor.vertexDescriptor = vertexDescriptor

        let renderPipelineState: MTLRenderPipelineState
        do {
            renderPipelineState = try device.makeRenderPipelineState(descriptor: pipelineDescriptor)
        } catch {
            DispatchQueue.main.async {
                self.result = .couldNotCreateRenderPipelineState
            }
            return
        }

        DispatchQueue.main.async {
            self.result = .success(Data(device: device,
                                        commandQueue: commandQueue,
                                        commandBuffer: commandBuffer,
                                        library: library,
                                        pipelineDescriptor: pipelineDescriptor,
                                        pixelFormat: pixelFormat,
                                        renderPipelineState: renderPipelineState))
        }
    }
}
