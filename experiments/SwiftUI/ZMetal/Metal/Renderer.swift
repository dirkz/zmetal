//
//  Renderer.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import Foundation
import MetalKit

class Renderer: NSObject {
    let metalConfig: MetalConfigurator.Data

    init(config: MetalConfigurator.Data) {
        self.metalConfig = config
    }
}

extension Renderer: MTKViewDelegate {
    func mtkView(_ view: MTKView, drawableSizeWillChange size: CGSize) {
    }

    func draw(in view: MTKView) {
        let encoder = metalConfig.makeRenderCommandEncoder(view: view)

        guard let drawable = view.currentDrawable else {
            preconditionFailure("no currentDrawable")
        }

        let viewMatrix = float4x4(translationBy: float3(0, 0, -2))
        let modelMatrix = float4x4(rotationAbout: float3(0, 1, 0), by: -Float.pi / 6) *  float4x4(scaleBy: 2)
        let modelViewMatrix = viewMatrix * modelMatrix

        let aspectRatio = Float(view.drawableSize.width / view.drawableSize.height)
        let projectionMatrix = float4x4(perspectiveProjectionFov: Float.pi / 3,
                                        aspectRatio: aspectRatio,
                                        nearZ: 0.1,
                                        farZ: 100)

        //var uniforms = Uniforms(modelViewMatrix: modelViewMatrix, projectionMatrix: projectionMatrix)

        metalConfig.commandBuffer.present(drawable)
        encoder.endEncoding()
        metalConfig.commandBuffer.commit()
    }
}
