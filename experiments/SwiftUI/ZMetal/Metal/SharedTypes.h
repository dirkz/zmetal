//
//  SharedTypes.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#ifndef SharedTypes_h
#define SharedTypes_h

#ifndef __METAL_VERSION__
#include <simd/simd.h>
using namespace simd;
#else
using namespace metal;
#endif

#ifndef __METAL_VERSION__
#define CONST(t, n, v) constexpr t n = v
#else
#define CONST(t, n, v) constant t n = v
#endif

CONST(int, BUFFER_VERTICES, 0);
CONST(int, BUFFER_INDICES, 1);
CONST(int, BUFFER_UNIFORMS, 2);

struct Uniforms {
    float4x4 modelViewMatrix;
    float4x4 projectionMatrix;
};

#endif /* SharedTypes_h */
