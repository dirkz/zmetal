//
//  MetalDispatchView.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import SwiftUI
import Metal

struct MetalDispatchView: View {
    @StateObject private var metalConfig = MetalConfigurator()

    var body: some View {
        switch metalConfig.result {
        case .none:
            Text("Initializing ...")
                .task(metalConfig.configure)
        case .noDevice:
            Text("No metal device")
        case .noCommandQueue:
            Text("No command queue")
        case .noCommandBuffer:
            Text("No command buffer")
        case .noDefaultLibrary:
            Text("No default library")
        case .noVertexFunction:
            Text("No vertex function")
        case .noFragmentFunction:
            Text("No fragment function")
        case .couldNotCreateRenderPipelineState:
            Text("Could not create the render pipeline state")
        case .success(let data):
            Text("Success")
            MetalView(metalConfig: data)
        }
    }
}

#Preview {
    MetalDispatchView()
}
