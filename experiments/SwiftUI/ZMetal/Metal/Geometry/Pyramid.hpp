//
//  Pyramid.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#ifndef Pyramid_hpp
#define Pyramid_hpp

#include <vector>

#include "Geometry.hpp"

namespace z::metal {

struct Pyramid: public z::metal::Geometry {
    Pyramid(float a);

    virtual std::vector<simd::float3>& vertices();
    virtual std::vector<uint16_t>& indices();
    virtual std::vector<simd::float2>& texture_coordinates();

private:
    std::vector<simd::float3> _vertices;
    std::vector<uint16_t> _indices;
    std::vector<simd::float2> _texture_coordinates;
};

}

#endif /* Pyramid_hpp */
