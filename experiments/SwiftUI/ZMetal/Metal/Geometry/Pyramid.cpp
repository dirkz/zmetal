//
//  Pyramid.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#include "Pyramid.hpp"

using simd::float2;
using simd::float3;

namespace z::metal {

Pyramid::Pyramid(float a) {
    float altitude = sqrtf(3) / 2 * a;
    auto bottomA = float3{-a/2, 0, a/2};
    auto bottomB = float3{a/2, 0, a/2};
    auto bottomC = float3{0, 0, -a/2};
    auto top = float3{0, 0, altitude};
    _vertices = {bottomA, bottomB, bottomC, top};
    uint16_t iA = 0;
    uint16_t iB = 1;
    uint16_t iC = 2;
    uint16_t iTop = 3;
    _indices = {iC, iB, iA, iTop, iC, iA, iTop, iA, iB, iTop, iB, iC};
    auto tA = float2{0, 1};
    auto tB = float2{1, 1};
    auto tC = float2{0.5, 0};
    _texture_coordinates = {tA, tB, tC}; // TODO
};

std::vector<float3>& Pyramid::vertices() {
    return this->_vertices;
}

std::vector<uint16_t>& Pyramid::indices() {
    return this->_indices;
}

std::vector<float2>& Pyramid::texture_coordinates() {
    return this->_texture_coordinates;
}

}
