//
//  Geometry+Metal.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#define NS_PRIVATE_IMPLEMENTATION
#define CA_PRIVATE_IMPLEMENTATION
#define MTL_PRIVATE_IMPLEMENTATION
#include <Metal/Metal.hpp>

#include "Geometry+Metal.hpp"

namespace z::metal {

template<class T>
MTL::Buffer *makeBuffer(MTL::Device& device, std::vector<T>& elements) {
    constexpr size_t elem_size = sizeof(elements[0]);
    size_t len = elements.size() * elem_size;
    MTL::Buffer *buffer = device.newBuffer(len, MTL::ResourceStorageModeShared);
    memcpy(buffer->contents(), elements.data(), len);
    buffer->didModifyRange(NS::Range{0, len});
    return buffer->autorelease();
}

MTL::Buffer *makeVertexBuffer(MTL::Device& device, Geometry& geometry) {
    return makeBuffer(device, geometry.vertices());
}

MTL::Buffer *makeIndexBuffer(MTL::Device& device, Geometry& geometry) {
    return makeBuffer(device, geometry.indices());
}

MTL::Buffer *makeTextureCoordinatesBuffer(MTL::Device& device, Geometry& geometry) {
    return makeBuffer(device, geometry.texture_coordinates());
}

}
