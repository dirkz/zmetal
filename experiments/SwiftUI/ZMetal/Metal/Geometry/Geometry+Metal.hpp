//
//  Geometry+Metal.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#ifndef Geometry_Metal_hpp
#define Geometry_Metal_hpp

#include <Metal/Metal.hpp>

#include "Geometry.hpp"

namespace z::metal {

MTL::Buffer *makeVertexBuffer(MTL::Device& device, Geometry& geometry);
MTL::Buffer *makeIndexBuffer(MTL::Device& device, Geometry& geometry);
MTL::Buffer *makeTextureCoordinatesBuffer(MTL::Device& device, Geometry& geometry);

}

#endif /* Geometry_Metal_hpp */
