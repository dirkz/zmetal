//
//  Geometry.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#ifndef Geometry_hpp
#define Geometry_hpp

#include <vector>
#include <simd/simd.h>

namespace z::metal {

struct Geometry {
    virtual std::vector<simd::float3>& vertices() = 0;
    virtual std::vector<uint16_t>& indices() = 0;
    virtual std::vector<simd::float2>& texture_coordinates() = 0;

    virtual ~Geometry() {};
};

}

#endif /* Geometry_hpp */
