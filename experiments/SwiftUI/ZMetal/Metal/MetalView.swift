//
//  MetalViewRepresentable.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import SwiftUI
import MetalKit

struct MetalView: NSViewRepresentable {
    typealias NSViewType = MTKView

    let metalConfig: MetalConfigurator.Data

    func makeNSView(context: Context) -> MTKView {
        return createView(context: context)
    }
    
    func updateNSView(_ uiView: MTKView, context: Context) {
    }

    func makeCoordinator() -> RendererHolder {
        RendererHolder(renderer: Renderer(config: metalConfig))
    }
}


extension MetalView {
    func createView(context: Context) -> MTKView {
        let view = MTKView()
        view.colorPixelFormat = metalConfig.pixelFormat
        view.device = metalConfig.device
        view.clearColor = MTLClearColor(red: 1, green: 0, blue: 0, alpha: 1)
        view.enableSetNeedsDisplay = true
        view.delegate = context.coordinator.renderer
        return view
    }
}

class RendererHolder {
    let renderer: Renderer

    init(renderer: Renderer) {
        self.renderer = renderer
    }
}
