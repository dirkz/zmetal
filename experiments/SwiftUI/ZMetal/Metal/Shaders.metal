//
//  Shaders.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "SharedTypes.h"

struct VertexIn {
    float3 position [[attribute(BUFFER_VERTICES)]];
};

struct VertexOut {
    float4 position [[position]];
};

vertex VertexOut vertex_main(VertexIn vertexIn [[stage_in]],
                             constant Uniforms &uniforms [[buffer(BUFFER_UNIFORMS)]])
{
    VertexOut vertexOut;
    vertexOut.position = uniforms.projectionMatrix * uniforms.modelViewMatrix * float4(vertexIn.position, 1);
    return vertexOut;
}

fragment float4 fragment_main(VertexOut fragmentIn [[stage_in]]) {
    return float4(1, 0, 0, 1);
}
