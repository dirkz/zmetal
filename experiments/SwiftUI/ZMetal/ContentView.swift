//
//  ContentView.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            MetalDispatchView()
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
