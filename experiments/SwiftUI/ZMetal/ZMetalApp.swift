//
//  ZMetalApp.swift
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

import SwiftUI

@main
struct ZMetalApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
