//
//  SCNGeometry+Triangles.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 8/4/24.
//

#import <SceneKit/SceneKit.h>

@class ColoredTextMesh;

NS_ASSUME_NONNULL_BEGIN

@interface SCNGeometry (Triangles)

- (ColoredTextMesh *)vertexDataMeshWithDevice:(id<MTLDevice>)device;

@end

NS_ASSUME_NONNULL_END
