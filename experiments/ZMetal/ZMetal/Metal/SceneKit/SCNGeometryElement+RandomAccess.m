//
//  SCNGeometryElement+RandomAccess.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import "SCNGeometryElement+RandomAccess.h"

@implementation SCNGeometryElement (RandomAccess)

- (uint16_t)indexAt:(NSUInteger)index {
    return *(((const uint16_t *) self.data.bytes) + index);
}

@end
