//
//  SCNGeometryElement+RandomAccess.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <SceneKit/SceneKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SCNGeometryElement (RandomAccess)

- (uint16_t)indexAt:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
