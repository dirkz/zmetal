//
//  SCNGeometrySource+RandomAccess.mm
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import "SCNGeometrySource+RandomAccess.h"

@implementation SCNGeometrySource (RandomAccess)

- (simd::float3)float3At:(NSUInteger)index {
    const char *bytes = (const char *) self.data.bytes;
    const char *target = bytes + self.dataOffset + self.dataStride * index;
    const simd::float3 *p = (const simd::float3 *) target;
    return *p;
}

- (simd::float2)float2At:(NSUInteger)index {
    const char *bytes = (const char *) self.data.bytes;
    const char *target = bytes + self.dataOffset + self.dataStride * index;
    const simd::float2 *p = (const simd::float2 *) target;
    return *p;
}

@end
