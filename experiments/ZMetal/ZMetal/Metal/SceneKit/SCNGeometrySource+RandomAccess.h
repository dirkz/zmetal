//
//  SCNGeometrySource+RandomAccess.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <SceneKit/SceneKit.h>
#import <simd/simd.h>

NS_ASSUME_NONNULL_BEGIN

@interface SCNGeometrySource (RandomAccess)

- (simd::float3)float3At:(NSUInteger)index;
- (simd::float2)float2At:(NSUInteger)index;

@end

NS_ASSUME_NONNULL_END
