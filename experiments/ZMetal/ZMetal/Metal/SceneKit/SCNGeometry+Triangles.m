//
//  SCNGeometry+Triangles.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 8/4/24.
//

#import "SCNGeometry+Triangles.h"

#import "ColoredTextMesh.h"

@implementation SCNGeometry (Triangles)

- (ColoredTextMesh *)vertexDataMeshWithDevice:(id<MTLDevice>)device {
    return [[ColoredTextMesh alloc] initWithDevice:device geometry:self];
}

@end
