//
//  RayTracer.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "RayTracerSharedTypes.h"

ray create_ray(uint2 gid, device Camera& camera, uint2 textureSize) {
    // For every pixel in the texture, this is the amount
    // to move in the world to the right/left or up/down
    // along the world viewport.
    float dx = camera.viewport.x / static_cast<float>(textureSize.x);
    float dy = camera.viewport.y / static_cast<float>(textureSize.y);

    float3 camera_direction = camera.target - camera.origin;

    // Direction vector pointing along the positive x-axis of the world viewport.
    float3 u = normalize(cross(camera_direction, camera.up));

    // Direction vector pointing along the positive y-axis of the world viewport.
    // Note that this is in the opposite direction of the texture.
    // World coordinates are right handed, while in the texture positive y points down.
    float3 v = normalize(camera.up);

    // World viewport increments per texture pixel.
    float3 du = u * dx;
    float3 dv = v * dy;

    // The point in the middle of the world viewport.
    float3 pViewPortCenterLeft = camera.target - u * (camera.viewport.x/2);

    // Top left of the viewport in the world (Q).
    float3 pViewPortTopLeft = pViewPortCenterLeft + v * (camera.viewport.y/2);

    // The point on the viewport that we have to cast the ray through.
    // With the `du/2` and `dv/2` offsets we land "in the middle".
    float3 p = pViewPortTopLeft + (gid.x * du + du/2) - (gid.y * dv + dv/2);

    // The direction of the ray.
    float3 direction = p - camera.origin;

    return ray{camera.origin, direction};
}

kernel void raytracer_compute(uint2 gid [[thread_position_in_grid]],
                              device RayTracerArguments & args [[buffer(BufferIndexArguments)]]) {
    if ((gid.x >= args.target.get_width()) || (gid.y >= args.target.get_height())) {
        return;
    }

    ray r = create_ray(gid, args.camera, uint2{args.target.get_width(), args.target.get_height()});
    intersector<triangle_data> intersector;
    intersection_result<triangle_data> intersection = intersector.intersect(r, args.accelerationStructure);

    if (intersection.type == intersection_type::triangle) {
        args.target.write(float4{1, 0, 0, 1}, gid);
    } else {
        args.target.write(float4{0, 0, 0, 1}, gid);
    }
}

