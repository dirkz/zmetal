//
//  ComputeShader.mm
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#import <simd/simd.h>
#import <vector>

#import "RayTracerCompute.h"

#import "RayTracerSharedTypes.h"
#import "VertexBuffer.hpp"

using namespace simd;
using namespace z::metal;

@interface RayTracerCompute ()

@property (nonatomic) MTKView *view;
@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLLibrary> library;

@property (nonatomic) id<MTLBuffer> primitiveBuffer;
@property (nonatomic) id<MTLBuffer> vertexBuffer;
@property (nonatomic) id<MTLBuffer> indexBuffer;

@property (nonatomic) id<MTLComputePipelineState> computePipelineState;
@property (nonatomic) id<MTLTexture> texture;
@property (nonatomic) id<MTLBuffer> argumentBuffer;
@property (nonatomic) MTLSize threadgroupSize;
@property (nonatomic) MTLSize threadgroupCount;

@end

@implementation RayTracerCompute

- (instancetype)initView:(MTKView *)view
                  device:(id<MTLDevice>)device
                 library:(id<MTLLibrary>)library {
    self = [super init];
    if (self) {
        _view = view;
        _device = device;
        _library = library;

        MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
        textureDescriptor.textureType = MTLTextureType2D;
        textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
        textureDescriptor.width = view.drawableSize.width;
        textureDescriptor.height = view.drawableSize.height;
        textureDescriptor.usage = MTLTextureUsageShaderWrite | MTLTextureUsageShaderRead;
        textureDescriptor.resourceOptions = MTLStorageModeShared;

        _texture = [device newTextureWithDescriptor:textureDescriptor];

        _threadgroupSize = MTLSizeMake(16, 16, 1);
        _threadgroupCount.width = (_texture.width  + _threadgroupSize.width -  1) / _threadgroupSize.width;
        _threadgroupCount.height = (_texture.height + _threadgroupSize.height - 1) / _threadgroupSize.height;
        _threadgroupCount.depth = 1;

        NSAssert(_device.argumentBuffersSupport != MTLArgumentBuffersTier1,
                 @"Metal 3 argument buffers are suppported only on Tier2 devices");

        _argumentBuffer = [_device newBufferWithLength:sizeof(RayTracerArguments)
                                               options:MTLResourceStorageModeShared];
    }
    return self;
}

- (BOOL)buildComputePipelineState {
    id<MTLFunction> kernelFunction = [self.library newFunctionWithName:@"raytracer_compute"];
    if (!kernelFunction) {
        NSLog(@"no compute function");
        return NO;
    }

    NSError *error = nil;
    self.computePipelineState = [self.device newComputePipelineStateWithFunction:kernelFunction
                                                                           error:&error];
    if (!self.computePipelineState) {
        NSLog(@"no compute pipeline state: %@", error);
        return NO;
    }

    return YES;
}

- (void)buildGeometryBuffersDevice {
    float3 v_top{0, 5, 0};
    float3 v_bl{-5, -5, 0};
    float3 v_br{5, -5, 0};
    float3 normal = cross(v_top - v_bl, v_br - v_bl);
    float4 color{1, 0, 0, 1};
    float2 t_top{0.5, 0};
    float2 t_bl{0, 1};
    float2 t_br{1, 1};

    std::vector<Triangle>triangles{ {{v_top, v_bl, v_br}, normal, {t_top, t_bl, t_br}, color} };
    VertexBuffer<VertexPosition, uint16_t> vb;
    for (const Triangle &t : triangles) {
        for (int i = 0; i < 3; ++i) {
            VertexPosition vd{t.vertices[i]};
            vb.push_back(vd);
        }
    }

    self.primitiveBuffer = [self.device
                            newBufferWithBytes:&triangles
                            length:sizeof(Triangle)
                            options:MTLResourceStorageModeShared];

    self.vertexBuffer = [self.device
                         newBufferWithBytes:vb.vertices().data()
                         length:vb.vertex_data_size()
                         options:MTLResourceStorageModeShared];

    self.indexBuffer = [self.device
                        newBufferWithBytes:vb.index_data()
                        length:vb.index_data_size()
                        options:MTLResourceStorageModeShared];

    self.triangleCount = triangles.size();
}

- (void)buildAccelerationStructureCommandBuffer:(id<MTLCommandBuffer>)commandBuffer {
    [self buildGeometryBuffersDevice];

    MTLAccelerationStructureTriangleGeometryDescriptor *triangles =
    [MTLAccelerationStructureTriangleGeometryDescriptor descriptor];

    triangles.triangleCount = self.triangleCount;

    triangles.indexBuffer = self.indexBuffer;
    triangles.indexType = MTLIndexTypeUInt16;
    triangles.indexBufferOffset = 0;

    triangles.vertexBuffer = self.vertexBuffer;
    triangles.vertexFormat = MTLAttributeFormatFloat3;
    triangles.vertexBufferOffset = 0;
    triangles.vertexStride = sizeof(VertexPosition);

    triangles.primitiveDataBuffer = self.primitiveBuffer;
    triangles.primitiveDataElementSize = sizeof(Triangle);
    triangles.primitiveDataStride = sizeof(Triangle);

    MTLPrimitiveAccelerationStructureDescriptor *primitive =
    [MTLPrimitiveAccelerationStructureDescriptor descriptor];

    primitive.geometryDescriptors = @[triangles];

    self.accelerationStructure = [self.device
                                  newAccelerationStructureWithDescriptor:primitive];

    MTLAccelerationStructureSizes sizes = [self.device
                                           accelerationStructureSizesWithDescriptor:primitive];

    id<MTLBuffer> scratchBuffer = [self.device
                                   newBufferWithLength:sizes.buildScratchBufferSize
                                   options:MTLStorageModeShared];

    id<MTLAccelerationStructureCommandEncoder> accCommandEncoder =
    [commandBuffer accelerationStructureCommandEncoder];

    [accCommandEncoder buildAccelerationStructure:self.accelerationStructure
                                       descriptor:primitive
                                    scratchBuffer:scratchBuffer
                              scratchBufferOffset:0];

    [accCommandEncoder endEncoding];
}

- (void)traceRays:(id<MTLCommandBuffer>)commandBuffer {
    float3 origin{0, 0, 5};
    float3 target{0, 0, 1};
    float viewAspect = self.texture.width / self.texture.height;
    float vpWidth = 20;
    float vpHeight = vpWidth / viewAspect;
    float2 viewport{vpWidth, vpHeight};
    float3 up{0, 1, 0};
    Camera camera{origin, target, up, viewport};

    RayTracerArguments *args = (RayTracerArguments *) self.argumentBuffer.contents;
    args->target = self.texture.gpuResourceID;
    args->camera = camera;
    args->accelerationStructure = self.accelerationStructure.gpuResourceID;

    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];
    [computeEncoder setComputePipelineState:_computePipelineState];
    [computeEncoder setBuffer:self.argumentBuffer offset:0 atIndex:BufferIndexArguments];

    [computeEncoder useResource:self.texture
                          usage:MTLResourceUsageRead | MTLResourceUsageWrite];

    [computeEncoder useResource:self.accelerationStructure
                          usage:MTLResourceUsageRead];

    [computeEncoder dispatchThreadgroups:_threadgroupCount
                   threadsPerThreadgroup:_threadgroupSize];

    [computeEncoder endEncoding];
}

@end
