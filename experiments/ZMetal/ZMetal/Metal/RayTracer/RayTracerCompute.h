//
//  ComputeShader.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RayTracerCompute : NSObject

@property (nonatomic) NSUInteger triangleCount;
@property (nonatomic) id<MTLAccelerationStructure> accelerationStructure;

- (instancetype)initView:(nonnull MTKView *)view
                  device:(id<MTLDevice>)device
                 library:(id<MTLLibrary>)library;

- (BOOL)buildComputePipelineState;

- (void)buildAccelerationStructureCommandBuffer:(id<MTLCommandBuffer>)commandBuffer;

- (void)traceRays:(id<MTLCommandBuffer>)commandBuffer;

@end

NS_ASSUME_NONNULL_END
