//
//  RayTracer.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RayTracer : NSObject <MTKViewDelegate>

@end

NS_ASSUME_NONNULL_END
