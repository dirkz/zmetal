//
//  RayTracerSharedTypes.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#ifndef RayTracerSharedTypes_h
#define RayTracerSharedTypes_h

#ifndef __METAL_VERSION__

#include <simd/simd.h>
using namespace simd;

enum class access {
    write,
    read
};

template<typename T, enum access a>
struct texture2d : public MTLResourceID {
    texture2d(MTLResourceID v) : MTLResourceID(v) {}
};

struct sampler : public MTLResourceID {
    sampler(MTLResourceID v) : MTLResourceID(v) {}
};

struct primitive_acceleration_structure : public MTLResourceID {
    primitive_acceleration_structure(MTLResourceID v) : MTLResourceID(v) {}
};

#define DEVICE

#else

using namespace metal;
using namespace metal::raytracing;

#define DEVICE device

#endif

enum BufferIndex {
    BufferIndexArguments
};

struct Triangle {
    float3 vertices[3];
    float3 normal;
    float2 tex_coordinates[3];
    float4 color;
};

struct VertexPosition {
    float3 position;
};

#ifndef __METAL_VERSION__

inline bool operator<(const VertexPosition& v1, const VertexPosition& v2) {
    return
    v1.position.x < v2.position.x ||
    v1.position.y < v2.position.y ||
    v1.position.z < v2.position.z;
}

#endif

/// Ray tracing camera
/// @Note Right-handed coordinate system
struct Camera {
    /// Where the camera stands.
    float3 origin;
    /// Where the camera is aimed at.
    float3 target;
    float3 up;
    /// Viewport _dimensions_ in world coordinates
    float2 viewport;
};

struct RayTracerArguments {
    texture2d<float, access::write> target;
    Camera camera;
    primitive_acceleration_structure accelerationStructure;
};

#endif /* RayTracerSharedTypes_h */
