//
//  RayTracer.mm
//  ZMetal
//
//  Created by Dirk Zimmermann on 12/4/24.
//

#import "RayTracer.h"

#import "RayTracerSharedTypes.h"
#import "RayTracerCompute.h"

@interface RayTracer ()

@property (nonatomic) BOOL isInitialized;

@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLLibrary> library;

@property (nonatomic) RayTracerCompute *computeShader;

@property (nonatomic) id<MTLCommandBuffer> commandBuffer;
@property (nonatomic) id<MTLCommandQueue> commandQueue;

@property (nonatomic) id<MTLDepthStencilState> depthState;

@end

@implementation RayTracer {
    dispatch_queue_t _once;
}

#pragma mark - MTKViewDelegate

- (void)drawInMTKView:(nonnull MTKView *)view {
    if (![self checkInitialized:view]) {
        return;
    }

    MTLRenderPassDescriptor *renderPassDescriptor = [view currentRenderPassDescriptor];
    if (!renderPassDescriptor) {
        NSLog(@"no render pass descriptor");
        return;
    }

    id<CAMetalDrawable> drawable = view.currentDrawable;
    if (!drawable) {
        NSLog(@"no current drawable");
        return;
    }

    [_computeShader traceRays:self.commandBuffer];

    id<MTLRenderCommandEncoder> renderCommandEncoder = [self.commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    if (!renderCommandEncoder) {
        NSLog(@"no render command encoder");
        return;
    }

    [renderCommandEncoder setDepthStencilState:_depthState];

    [renderCommandEncoder endEncoding];

    [self.commandBuffer presentDrawable:drawable];
    [self.commandBuffer commit];
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
}

#pragma mark - Other

- (BOOL)checkInitialized:(nonnull MTKView *)view {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _once = dispatch_queue_create("once", DISPATCH_QUEUE_SERIAL);
        dispatch_async(_once, ^{
            BOOL isInitialized = [self createBasicsView:view];

            if (!isInitialized) {
                return;
            }

            self.commandBuffer = [self.commandQueue commandBuffer];

            self.computeShader = [[RayTracerCompute alloc] initView:view
                                                             device:self.device
                                                            library:self.library];

            if (!self.computeShader) {
                return;
            }

            isInitialized = [self.computeShader buildComputePipelineState];
            
            if (!isInitialized) {
                return;
            }

            [self.computeShader buildAccelerationStructureCommandBuffer:self.commandBuffer];

            self.isInitialized = isInitialized;
        });
    });
    return _isInitialized;
}

- (BOOL)createBasicsView:(nonnull MTKView *)view {
    _device = MTLCreateSystemDefaultDevice();
    if (!_device) {
        NSLog(@"could not create the default device");
        return NO;
    }
    view.device = _device;

    _library = [_device newDefaultLibrary];
    if (!_library) {
        NSLog(@"no default library");
        return NO;
    }

    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm;
    view.depthStencilPixelFormat = MTLPixelFormatDepth16Unorm;
    view.clearDepth = 1.0;

    MTLDepthStencilDescriptor *depthDescriptor = [MTLDepthStencilDescriptor new];
    depthDescriptor.depthCompareFunction = MTLCompareFunctionLessEqual;
    depthDescriptor.depthWriteEnabled = YES;
    _depthState = [_device newDepthStencilStateWithDescriptor:depthDescriptor];

    _commandQueue = [_device newCommandQueue];

    return YES;
}

@end
