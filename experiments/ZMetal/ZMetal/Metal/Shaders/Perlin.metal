//
//  Perlin.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 6/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "Perlin.h"

namespace z {
    namespace metal {
        namespace perlin {

            template<class V>
            V lookup_gradient(constant V *gradients,
                              const int2 dimensions,
                              int x,
                              int y) {
                return gradients[y * dimensions.x + x];
            }

            template<class V, class T>
            void noise(constant V *gradients,
                       const int2 dimensions,
                       const V v) {
                int2 cube = cube_coordinates(dimensions, v);
                const V g000 = lookup_gradient(gradients, dimensions, cube.x + 0, cube.y + 0);
                const V g100 = lookup_gradient(gradients, dimensions, cube.x + 1, cube.y + 0);
                const V g010 = lookup_gradient(gradients, dimensions, cube.x + 0, cube.y + 1);
                const V g110 = lookup_gradient(gradients, dimensions, cube.x + 1, cube.y + 1);

                T nearestX = v.x > 0 ? floor(v.x) : ceil(v.x);
                T nearestY = v.y > 0 ? floor(v.y) : ceil(v.y);

                T offsetX = v.x - nearestX;
                T offsetY = v.y - nearestY;
            }

        } // namespace perlin
    } // namespace metal
} // namespace z
