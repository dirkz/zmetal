//
//  Quad.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "SharedTypes.h"

struct Vertex {
    float3 position [[attribute(ArgIndexPosition)]];
    float2 texCoords [[attribute(ArgIndexTexCoord)]];
};

struct Fragment {
    float4 position [[position]];
    float2 texCoords;
};

kernel void quad_compute(texture2d<float, access::write>texture [[texture(TextureIndexQuad)]],
                         uint2 gid [[thread_position_in_grid]]) {
    if ((gid.x >= texture.get_width()) || (gid.y >= texture.get_height())) {
        return;
    }
    float x = (float) gid.x / texture.get_width();
    float y = (float) gid.y / texture.get_width();
    float c = (x + y) / 2;
    float4 color{c, c, 0, 1};
    texture.write(color, gid);
}

vertex Fragment quad_vertex(Vertex vert [[stage_in]]) {
    float4 position = float4(vert.position, 1);
    Fragment frag{position, vert.texCoords};
    return frag;
}

fragment float4 quad_fragment(Fragment frag [[stage_in]],
                              texture2d<float>texture [[texture(TextureIndexQuad)]]) {
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear);

    const float4 color = texture.sample(textureSampler, frag.texCoords);
    return color;
}
