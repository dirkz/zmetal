//
//  SharedTypes.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 2/4/24.
//

#ifndef SharedTypes_h
#define SharedTypes_h

#ifndef __METAL_VERSION__
#include <simd/simd.h>
using namespace simd;
#else
using namespace metal;
#endif

typedef enum _ArgIndex {
    ArgIndexPosition,
    ArgIndexNormal,
    ArgIndexColor,
    ArgIndexTexCoord
} ArgIndex;

typedef enum _BufferIndex {
    BufferIndexVertex,
    BufferIndexUniformProjection,
    BufferIndexPrimitiveAccelerationStructure,
    BufferIndexCamera
} BufferIndex;

typedef enum _TextureIndex {
    TextureIndexQuad,
} TextureIndex;

struct Projection {
    float4x4 modelView;
    float4x4 projection;
    float3x3 normal;
};

/// Path tracing camera
/// @Note Right-handed coordinate system
struct Camera {
    /// Where the camera stands.
    float3 origin;
    /// Where the camera is aimed at.
    float3 target;
    float3 up;
    /// Viewport _dimensions_ in world coordinates
    float2 viewport;
};

#endif /* SharedTypes_h */
