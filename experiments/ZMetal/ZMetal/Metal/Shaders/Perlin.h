//
//  Perlin.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 6/4/24.
//

#ifndef Perlin_h
#define Perlin_h

#ifndef __METAL_VERSION__
#include <simd/simd.h>
using namespace simd;
#else
using namespace metal;
#endif

namespace z {
namespace metal {
namespace perlin {

template<class T>
float clamp(T x, T lowerlimit = 0.0, T upperlimit = 1.0f) {
    if (x < lowerlimit) {
        return lowerlimit;
    }
    if (x > upperlimit) {
        return upperlimit;
    }
    return x;
}

template<class T>
T smootherstep(T edge0, T edge1, T x) {
    // Scale, and clamp x to 0..1 range
    x = clamp((x - edge0) / (edge1 - edge0));

    return x * x * x * (x * (6.0 * x - 15.0) + 10.0);
}

template<class V>
int2 cube_coordinates(const int2 dimensions, const V v) {
    int ix = v.x > 0 ?
    static_cast<int>(floor(fmod(v.x, dimensions.x - 1))) :
    static_cast<int>(ceil(fmod(v.x, dimensions.x - 1)));

    int iy = v.y > 0 ?
    static_cast<int>(floor(fmod(v.y, dimensions.y - 1))) :
    static_cast<int>(ceil(fmod(v.y, dimensions.y - 1)));

    return int2{ix, iy};
}

} // namespace perlin
} // namespace metal
} // namespace z

#endif /* Perlin_h */
