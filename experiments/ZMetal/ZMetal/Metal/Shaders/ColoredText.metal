//
//  Shaders.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 4/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "SharedTypes.h"

struct Vertex {
    float3 position [[attribute(ArgIndexPosition)]];
    float3 normal [[attribute(ArgIndexNormal)]];
    float4 color [[attribute(ArgIndexColor)]];
    float2 texCoords [[attribute(ArgIndexTexCoord)]];
};

struct Fragment {
    float4 position [[position]];
    float4 color;
    float3 normal;
};

vertex Fragment colored_text_vertex(Vertex vert [[stage_in]],
                                    constant Projection &uniforms [[buffer(BufferIndexUniformProjection)]]) {
    Fragment frag;
    float4 position_homogenous = float4(vert.position, 1);
    frag.position = uniforms.projection * uniforms.modelView * position_homogenous;
    frag.color = vert.color;
    frag.normal = uniforms.normal * vert.normal;
    return frag;
}

fragment float4 colored_text_fragment(Fragment frag [[stage_in]]) {
    return frag.color;
}
