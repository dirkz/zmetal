//
//  PathTracer.metal
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#include <metal_stdlib>
using namespace metal;
using namespace metal::raytracing;

#include "SharedTypes.h"

ray create_ray(uint2 gid, constant Camera &camera, uint2 textureSize) {
    // For every pixel in the texture, this is the amount
    // to move in the world to the right/left or up/down
    // along the world viewport.
    float dx = camera.viewport.x / static_cast<float>(textureSize.x);
    float dy = camera.viewport.y / static_cast<float>(textureSize.y);

    float3 camera_direction = camera.target - camera.origin;

    // Direction vector pointing along the positive x-axis of the world viewport.
    float3 u = normalize(cross(camera_direction, camera.up));

    // Direction vector pointing along the positive y-axis of the world viewport.
    // Note that this is in the opposite direction of the texture.
    // World coordinates are right handed, while in the texture positive y points down.
    float3 v = normalize(camera.up);

    // World viewport increments per texture pixel.
    float3 du = u * dx;
    float3 dv = v * dy;

    // The point in the middle of the world viewport.
    float3 pViewPortCenterLeft = camera.target - u * (camera.viewport.x/2);

    // Top left of the viewport in the world (Q).
    float3 pViewPortTopLeft = pViewPortCenterLeft + v * (camera.viewport.y/2);

    // The point on the viewport that we have to cast the ray through.
    // With the `du/2` and `dv/2` offsets we land "in the middle".
    float3 p = pViewPortTopLeft + (gid.x * du + du/2) - (gid.y * dv + dv/2);

    // The direction of the ray.
    float3 direction = p - camera.origin;

    return ray{camera.origin, direction};
}

kernel void ray_compute(uint2 gid [[thread_position_in_grid]],
                        constant Camera &camera [[buffer(BufferIndexCamera)]],
                        texture2d<float, access::write>texture [[texture(TextureIndexQuad)]],
                        primitive_acceleration_structure accelerationStructure [[buffer(BufferIndexPrimitiveAccelerationStructure)]]) {
    if ((gid.x >= texture.get_width()) || (gid.y >= texture.get_height())) {
        return;
    }

    ray r = create_ray(gid, camera, uint2{texture.get_width(), texture.get_height()});
    intersector<triangle_data> intersector;
    intersection_result<triangle_data> intersection = intersector.intersect(r, accelerationStructure);

    if (intersection.type == intersection_type::triangle) {
        texture.write(float4{1, 0, 0, 1}, gid);
    } else {
        texture.write(float4{0, 0, 0, 1}, gid);
    }
}

struct Vertex {
    float3 position [[attribute(ArgIndexPosition)]];
    float2 texCoords [[attribute(ArgIndexTexCoord)]];
};

struct Fragment {
    float4 position [[position]];
    float2 texCoords;
};

vertex Fragment ray_vertex(Vertex vert [[stage_in]]) {
    float4 position = float4(vert.position, 1);
    Fragment frag{position, vert.texCoords};
    return frag;
}

fragment float4 ray_fragment(Fragment frag [[stage_in]],
                              texture2d<float>texture [[texture(TextureIndexQuad)]]) {
    constexpr sampler textureSampler (mag_filter::linear,
                                      min_filter::linear);

    const float4 color = texture.sample(textureSampler, frag.texCoords);
    return color;
}
