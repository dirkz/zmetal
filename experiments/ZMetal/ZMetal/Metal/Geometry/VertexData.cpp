//
//  VertexData.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 8/4/24.
//

#include "VertexData.hpp"

namespace z::metal {

std::pair<simd::float3, simd::float3> vertex_data_bounds(std::vector<VertexData> vs) {
    const float mininmum = std::numeric_limits<float>::min();
    const float maximum = std::numeric_limits<float>::max();
    simd::float3 lowest{maximum, maximum, maximum};
    simd::float3 highest{mininmum, mininmum, mininmum};
    for (auto v : vs) {
        lowest.x = std::min(v.position.x, lowest.x);
        lowest.y = std::min(v.position.y, lowest.y);
        lowest.z = std::min(v.position.z, lowest.z);
        highest.x = std::max(v.position.x, highest.x);
        highest.y = std::max(v.position.y, highest.y);
        highest.z = std::max(v.position.z, highest.z);
    }

    return std::make_pair(lowest, highest);
}

bool operator<(const VertexData& v1, const VertexData& v2) {
    return
    v1.position.x < v2.position.x ||
    v1.position.y < v2.position.y ||
    v1.position.z < v2.position.z ||
    v1.normal.x < v2.normal.x ||
    v1.normal.y < v2.normal.y ||
    v1.normal.z < v2.normal.z ||
    v1.color.r < v2.color.r ||
    v1.color.g < v2.color.g ||
    v1.color.b < v2.color.b ||
    v1.color.a < v2.color.a ||
    v1.tex_coords.x < v2.tex_coords.x ||
    v1.tex_coords.y < v2.tex_coords.y;
}

bool operator==(const VertexData& v1, const VertexData& v2) {
    return
    v1.position.x == v2.position.x &&
    v1.position.y == v2.position.y &&
    v1.position.z == v2.position.z &&
    v1.normal.x == v2.normal.x &&
    v1.normal.y == v2.normal.y &&
    v1.normal.z == v2.normal.z &&
    v1.color.r == v2.color.r &&
    v1.color.g == v2.color.g &&
    v1.color.b == v2.color.b &&
    v1.color.a == v2.color.a &&
    v1.tex_coords.x == v2.tex_coords.x &&
    v1.tex_coords.y == v2.tex_coords.y;
}

} // namespace z::metal
