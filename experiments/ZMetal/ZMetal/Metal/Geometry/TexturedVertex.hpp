//
//  TexturedVertex.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#ifndef TexturedVertex_hpp
#define TexturedVertex_hpp

#include <simd/simd.h>

namespace z::metal {

struct TexturedVertex {
    simd::float3 position;
    simd::float2 tex_coords;
};

bool operator<(const TexturedVertex& v1, const TexturedVertex& v2);

} // namespace z::metal

#endif /* TexturedVertex_hpp */
