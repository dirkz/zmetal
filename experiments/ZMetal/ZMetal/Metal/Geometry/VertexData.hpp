//
//  VertexData.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 8/4/24.
//

#ifndef Vertex_hpp
#define Vertex_hpp

#include <simd/simd.h>
#include <vector>
#include <tuple>

namespace z::metal {

struct VertexData {
    simd::float3 position;
    simd::float3 normal;
    simd::float2 tex_coords;
    simd::float4 color;
};

std::pair<simd::float3, simd::float3> vertex_data_bounds(std::vector<VertexData> vs);

bool operator<(const VertexData& v1, const VertexData& v2);
bool operator==(const VertexData& v1, const VertexData& v2);

} // namespace z::metal

#endif /* Vertex_hpp */
