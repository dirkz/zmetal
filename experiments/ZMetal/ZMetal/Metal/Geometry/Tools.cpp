//
//  Tools.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 5/4/24.
//

#include "Tools.hpp"

#include <stdlib.h>
#include <limits>

namespace z::metal {

template<class T>
T random() {
    uint32_t upper = std::numeric_limits<uint32_t>::max();
    uint32_t value = arc4random_uniform(upper);
    T fupper = static_cast<T>(upper);
    T fvalue = static_cast<T>(value);
    return fvalue / fupper;
}

float random_float() {
    return random<float>();
}

double random_double() {
    return random<double>();
}

simd::float4 random_color() {
    float r = random_float();
    float g = random_float();
    float b = random_float();
    return simd::float4{r, g, b, 1};
}

}
