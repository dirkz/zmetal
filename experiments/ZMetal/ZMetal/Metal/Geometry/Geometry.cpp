//
//  Geometry.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 3/4/24.
//

#include <iostream>
#include <set>
#include <limits>
#include <simd/simd.h>

#include "Geometry.hpp"
#include "Tools.hpp"

using std::cerr;
using std::cout;
using simd::float3;
using simd::float4;

namespace z::metal {

void Geometry::update_bounds() {
    std::set<uint16_t> found_indices;
    for (auto indices : _indices) {
        for (auto index : indices) {
            found_indices.insert(index);
        }
    }

    const float mininmum = std::numeric_limits<float>::min();
    const float maximum = std::numeric_limits<float>::max();
    simd::float3 lowest{maximum, maximum, maximum};
    simd::float3 highest{mininmum, mininmum, mininmum};
    for (auto index : found_indices) {
        auto v = _vertices[index];

        lowest.x = std::min(v.x, lowest.x);
        lowest.y = std::min(v.y, lowest.y);
        lowest.z = std::min(v.z, lowest.z);

        highest.x = std::max(v.x, highest.x);
        highest.y = std::max(v.y, highest.y);
        highest.z = std::max(v.z, highest.z);
    }

    _origin = lowest;
    _size = simd::float3{highest.x - lowest.x, highest.y - lowest.y, highest.z - lowest.z};
}

bool Geometry::validate() const {
    if (!(_vertices.size() != _normals.size() || _texcoords.size())) {
        cerr << "size difference between vertices, normals and texcoords\n";
        return false;
    }

    std::set<uint16_t> found_indices;
    for (auto indices : _indices) {
        for (auto index : indices) {
            found_indices.insert(index);
        }
    }

    std::set<uint16_t> missing_indices;
    for (size_t i = 0; i < _vertices.size(); ++i) {
        if (found_indices.find(i) == found_indices.end()) {
            missing_indices.insert(i);
        }
    }
    if (missing_indices.size() > 0) {
        cout << missing_indices.size() << " missing indices\n";
        return false;
    }

    return true;
}

void Geometry::generateColorShades() {
    _colors.clear();
    const float4 unused_color{-1, -1, -1, -1};
    for (size_t i = 0; i < _vertices.size(); ++i) {
        _colors.push_back(unused_color);
    }

    const size_t num_vertices = 3; // triangles

    for (auto indices : _indices) {
        float4 active_color = random_color();
        for (size_t index = 0; index < indices.size(); ++index) {
            if (index > 0 && index % num_vertices == 0) {
                active_color = random_color();
            }
            size_t color_index = indices[index];
            float4 current_color = _colors[color_index];
            if (current_color.x == unused_color.x) {
                _colors[color_index] = active_color;
            }
        }
    }
}

std::ostream& operator<<(std::ostream& stream, const Geometry& geometry) {
    auto o = geometry.origin();
    stream << "origin (" << o.x << " " << o.y << " " << o.z << "), ";
    stream << "size (" << geometry.width() << " " << geometry.height() << " " << geometry.depth() << ")";
    return stream;
}

}
