//
//  Geometry.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 3/4/24.
//

#ifndef Geometry_hpp
#define Geometry_hpp

#include <simd/simd.h>
#include <vector>
#include <utility>
#include <iostream>

namespace z::metal {

struct Geometry {
    Geometry() :
    _indices{},
    _vertices{},
    _normals{},
    _colors{},
    _texcoords{},
    _origin{0, 0, 0},
    _size{0} {
        std::cout << "Geometry default constructor\n";
    }

    Geometry(std::vector<std::vector<uint16_t>> indices,
             std::vector<simd::float3> vertices,
             std::vector<simd::float3> normals,
             std::vector<simd::float4> colors,
             std::vector<simd::float2> texcoords) :
    _indices{indices},
    _vertices{vertices},
    _normals{normals},
    _colors{colors},
    _texcoords{texcoords} {
        std::cout << "Geometry constructor\n";
        update_bounds();
    }

    Geometry(Geometry &other) :
    _indices{other._indices},
    _vertices{other._vertices},
    _normals{other._normals},
    _colors{other._colors},
    _texcoords{other._texcoords} {
        std::cout << "Geometry copy constructor\n";
        update_bounds();
    }

    // The Obj-C compiler doesn't use this.
    // Neither for properties, nor for direct members.
    Geometry(Geometry &&other) :
    _indices{std::move(other._indices)},
    _vertices{std::move(other._vertices)},
    _normals{std::move(other._normals)},
    _colors{std::move(other._colors)},
    _texcoords{std::move(other._texcoords)} {
        std::cout << "Geometry move constructor\n";
        update_bounds();
    }

    // Preferred by the Obj-C compiler for properties and object members.
    Geometry& operator=(const Geometry& other) {
        std::cout << "Geometry operator=\n";
        if (this == &other) {
            return *this;
        }

        _indices = other._indices;
        _vertices = other._vertices;
        _normals = other._normals;
        _colors = other._colors;
        _texcoords = other._texcoords;

        update_bounds();

        return *this;
    }

    simd::float3 origin() const { return _origin; }
    float width() const { return _size.x; }
    float height() const { return _size.y; }
    float depth() const { return _size.z; }

    bool validate() const;
    void generateColorShades();

    std::vector<std::vector<uint16_t>>& indices() { return _indices; };
    std::vector<simd::float3>& vertices() { return _vertices; }
    std::vector<simd::float3>& normals() { return _normals; }
    std::vector<simd::float4>& colors() { return _colors; }
    std::vector<simd::float2>& texcoords() { return _texcoords; }

private:
    void update_bounds();

    std::vector<std::vector<uint16_t>> _indices;
    std::vector<simd::float3> _vertices;
    std::vector<simd::float3> _normals;
    std::vector<simd::float4> _colors;
    std::vector<simd::float2> _texcoords;
    simd::float3 _origin;
    simd::float3 _size;
};

std::ostream& operator<<(std::ostream& stream, const Geometry& geometry);

}

#endif /* Geometry_hpp */
