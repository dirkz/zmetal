//
//  ColoredVertex.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#ifndef ColoredVertex_hpp
#define ColoredVertex_hpp

#include <simd/simd.h>

namespace z::metal {

struct ColoredVertex {
    simd::float3 position;
    simd::float4 color;
};

bool operator<(const ColoredVertex& v1, const ColoredVertex& v2);

} // namespace z::metal

#endif /* ColoredVertex_hpp */
