//
//  ColoredVertex.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#include "ColoredVertex.hpp"

namespace z::metal {

bool operator<(const ColoredVertex& v1, const ColoredVertex& v2) {
    return
    v1.position.x < v2.position.x ||
    v1.position.y < v2.position.y ||
    v1.position.z < v2.position.z ||
    v1.color.x < v2.color.x ||
    v1.color.y < v2.color.y ||
    v1.color.z < v2.color.z ||
    v1.color.w < v2.color.w;
}

} // namespace z::metal
