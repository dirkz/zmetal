//
//  TexturedVertex.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#include "TexturedVertex.hpp"

namespace z::metal {

bool operator<(const TexturedVertex& v1, const TexturedVertex& v2) {
    return
    v1.position.x < v2.position.x ||
    v1.position.y < v2.position.y ||
    v1.position.z < v2.position.z ||
    v1.tex_coords.x < v2.tex_coords.x ||
    v1.tex_coords.y < v2.tex_coords.y;
}

} // namespace z::metal
