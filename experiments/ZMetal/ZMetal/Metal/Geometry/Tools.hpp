//
//  Tools.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 5/4/24.
//

#ifndef Tools_hpp
#define Tools_hpp

#include <simd/simd.h>

namespace z::metal {

template<class T>
T random();

float random_float();
double random_double();

simd::float4 random_color();

}

#endif /* Tools_hpp */
