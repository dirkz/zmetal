//
//  VertexBuffer.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#ifndef VertexBuffer_hpp
#define VertexBuffer_hpp

#include <vector>
#include <map>
#include <initializer_list>

namespace z::metal {

template<class T, class I>
struct VertexBuffer {
    VertexBuffer() {};

    VertexBuffer(std::initializer_list<T> list) {
        for (auto e : list) {
            push_back(e);
        }
    }

    void push_back(const T& e) {
        auto it = _elements.find(e);
        if (it == _elements.end()) {
            I i = static_cast<I>(_elements.size());
            _indices.push_back(i);
            _elements[e] = i;
        } else {
            _indices.push_back(it->second);
        }
    }

    std::vector<I> indices() {
        return _indices;
    }

    void reset_indices() {
        _indices.clear();
    }

    std::vector<T> vertices() {
        std::vector<T> result(_elements.size());
        for (auto element : _elements) {
            result[element.second] = element.first;
        }
        return result;
    }

    const I *index_data() {
        return _indices.data();
    }

    size_t num_vertices() {
        return _elements.size();
    }

    size_t num_indices() {
        return _indices.size();
    }

    size_t vertex_data_size() {
        return _elements.size() * sizeof(T);
    }

    size_t index_data_size() {
        return _indices.size() * sizeof(T);
    }

private:
    std::vector<I> _indices;
    std::map<T, I> _elements;
};

} // namespace z::metal

#endif /* VertexBuffer_hpp */
