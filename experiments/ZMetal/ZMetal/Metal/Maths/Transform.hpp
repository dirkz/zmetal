//
//  Transform.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 4/4/24.
//

#ifndef Transform_hpp
#define Transform_hpp

#include <simd/simd.h>

namespace z::metal::transform {

constexpr simd::float4x4 identity();
simd::float4x4 perspective(float fovRadians, float aspect, float znear, float zfar);
simd::float4x4 x_rotate(float angleRadians);
simd::float4x4 y_rotate(float angleRadians);
simd::float4x4 z_rotate(float angleRadians);
simd::float4x4 translate(const simd::float3& v);
simd::float4x4 scale(const simd::float3& v);
simd::float3x3 normal_transform(const simd::float4x4& m);

} // namespace z::metal::transform

#endif /* Transform_hpp */
