//
//  Vec3.hpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 11/4/24.
//

#ifndef Vec3_hpp
#define Vec3_hpp

#include <math.h>

namespace z::metal {

template<class T>
struct Vec3 {
    Vec3(): e{} {};
    Vec3(const T& x, const T& y, const T& z): e{x, y, z} {};

    const T& x() const { return e[0]; }
    const T& y() const { return e[1]; }
    const T& z() const { return e[2]; }

    const Vec3<T>& operator-=(const Vec3<T>& v) {
        e[0] -= v.e[0];
        e[1] -= v.e[1];
        e[2] -= v.e[2];
        return *this;
    }

    const Vec3<T>& operator+=(const Vec3<T>& v) {
        e[0] += v.e[0];
        e[1] += v.e[1];
        e[2] += v.e[2];
        return *this;
    }

    const Vec3<T>& operator*=(const Vec3<T>& v) {
        e[0] *= v.e[0];
        e[1] *= v.e[1];
        e[2] *= v.e[2];
        return *this;
    }

    const Vec3<T>& operator*=(const T& s) {
        e[0] *= s;
        e[1] *= s;
        e[2] *= s;
        return *this;
    }

    const Vec3<T>& operator/=(const T& s) {
        e[0] /= s;
        e[1] /= s;
        e[2] /= s;
        return *this;
    }

    void normalize() {
        *this /= length();
    }

    T length_squared() const {
        return e[0] * e[0] + e[1] * e[1] + e[2] * e[2];
    }

    T length() const {
        return sqrt(length_squared());
    }

private:
    T e[3];
};

template<class T>
Vec3<T> operator-(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> v{a};
    v -= b;
    return v;
}

template<class T>
Vec3<T> operator+(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> v{a};
    v += b;
    return v;
}

template<class T>
Vec3<T> operator*(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> v{a};
    v *= b;
    return v;
}

} // namespace z::metal

#endif /* Vec3_hpp */
