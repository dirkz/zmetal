//
//  Transform.cpp
//  ZMetal
//
//  Created by Dirk Zimmermann on 4/4/24.
//

#include "Transform.hpp"

using simd::float4;
using simd::float3;
using simd::float3x3;
using simd::inverse;
using simd::transpose;

namespace z::metal::transform {

constexpr simd::float4x4 identity() {
    return (simd::float4x4) {(float4) { 1.f, 0.f, 0.f, 0.f },
        (float4) { 0.f, 1.f, 0.f, 0.f },
        (float4) { 0.f, 0.f, 1.f, 0.f },
        (float4) { 0.f, 0.f, 0.f, 1.f } };
}

simd::float4x4 perspective(float fovRadians, float aspect, float znear, float zfar) {
    float ys = 1.f / tanf(fovRadians * 0.5f);
    float xs = ys / aspect;
    float zs = zfar / ( znear - zfar );
    return simd_matrix_from_rows((float4) { xs, 0.0f, 0.0f, 0.0f },
                                 (float4) { 0.0f, ys, 0.0f, 0.0f },
                                 (float4) { 0.0f, 0.0f, zs, znear * zs },
                                 (float4) { 0, 0, -1, 0 });
}

simd::float4x4 x_rotate(float angleRadians) {
    const float a = angleRadians;
    return simd_matrix_from_rows((float4) { 1.0f, 0.0f, 0.0f, 0.0f },
                                 (float4) { 0.0f, cosf( a ), sinf( a ), 0.0f },
                                 (float4) { 0.0f, -sinf( a ), cosf( a ), 0.0f },
                                 (float4) { 0.0f, 0.0f, 0.0f, 1.0f });
}

simd::float4x4 y_rotate(float angleRadians) {
    const float a = angleRadians;
    return simd_matrix_from_rows((float4) { cosf( a ), 0.0f, sinf( a ), 0.0f },
                                 (float4) { 0.0f, 1.0f, 0.0f, 0.0f },
                                 (float4) { -sinf( a ), 0.0f, cosf( a ), 0.0f },
                                 (float4) { 0.0f, 0.0f, 0.0f, 1.0f });
}

simd::float4x4 z_rotate(float angleRadians) {
    const float a = angleRadians;
    return simd_matrix_from_rows((float4) { cosf( a ), sinf( a ), 0.0f, 0.0f },
                                 (float4) { -sinf( a ), cosf( a ), 0.0f, 0.0f },
                                 (float4) { 0.0f, 0.0f, 1.0f, 0.0f },
                                 (float4) { 0.0f, 0.0f, 0.0f, 1.0f });
}

simd::float4x4 translate(const simd::float3& v) {
    const float4 col0 = { 1.0f, 0.0f, 0.0f, 0.0f };
    const float4 col1 = { 0.0f, 1.0f, 0.0f, 0.0f };
    const float4 col2 = { 0.0f, 0.0f, 1.0f, 0.0f };
    const float4 col3 = { v.x, v.y, v.z, 1.0f };
    return simd_matrix(col0, col1, col2, col3);
}

simd::float4x4 scale(const simd::float3& v) {
    return simd_matrix((float4) { v.x, 0, 0, 0 },
                       (float4) { 0, v.y, 0, 0 },
                       (float4) { 0, 0, v.z, 0 },
                       (float4) { 0, 0, 0, 1.0 });
}

simd::float3x3 normal_transform(const simd::float4x4& m) {
    float4 col0 = m.columns[0];
    float4 col1 = m.columns[1];
    float4 col2 = m.columns[2];

    float3 col3_0 = float3 {col0.x, col0.y, col0.z};
    float3 col3_1 = float3 {col1.x, col1.y, col1.z};
    float3 col3_2 = float3 {col2.x, col2.y, col2.z};

    float3x3 mn = simd_matrix(col3_0, col3_1, col3_2);

    return transpose(inverse(mn));
}

} // namespace z::metal::transform
