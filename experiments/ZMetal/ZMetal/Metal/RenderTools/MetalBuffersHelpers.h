//
//  MetalBuffersHelpers.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <Metal/Metal.h>
#import <simd/simd.h>
#import <array>

NS_ASSUME_NONNULL_BEGIN

template<class T>
id<MTLBuffer> bufferFromVector(id<MTLDevice>device, const std::vector<T>& elements) {
    return [device
            newBufferWithBytes:elements.data()
            length:elements.size() * sizeof(T)
            options:MTLStorageModeShared];
}

NS_ASSUME_NONNULL_END
