//
//  ColoredTextMesh.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <Foundation/Foundation.h>
#import <SceneKit/SceneKit.h>
#import <simd/simd.h>

NS_ASSUME_NONNULL_BEGIN

@class IndexBuffer;

@interface ColoredTextMesh : NSObject

@property (nonatomic) NSUInteger argumentIndexPosition;
@property (nonatomic) NSUInteger argumentIndexNormal;
@property (nonatomic) NSUInteger argumentIndexColor;
@property (nonatomic) NSUInteger argumentIndexTexCoord;

@property (nonatomic) NSUInteger vertexBufferIndex;

@property (nonatomic, readonly) MTLVertexDescriptor *vertexDescriptor;

@property (nonatomic, readonly) id<MTLBuffer> vertexBuffer;
@property (nonatomic, readonly) NSArray<IndexBuffer *> *indexBuffers;

@property (nonatomic, readonly) float width;
@property (nonatomic, readonly) float height;

@property (nonatomic, readonly) NSUInteger size;

- (instancetype)initWithDevice:(id<MTLDevice>)device
                      geometry:(SCNGeometry *)geometry;

- (instancetype)initWithDevice:(id<MTLDevice>)device
                    string:(NSString *)string
                  fontSize:(CGFloat)fontSize
            extrusionDepth:(CGFloat)extrusionDepth;

@end

NS_ASSUME_NONNULL_END
