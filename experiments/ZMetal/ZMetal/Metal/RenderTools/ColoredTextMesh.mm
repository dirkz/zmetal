//
//  ColoredTextMesh.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import "ColoredTextMesh.h"

#import "IndexBuffer.h"
#import "VertexBuffer.hpp"
#import "Tools.hpp"
#import "MetalBuffersHelpers.h"
#import "SCNGeometryElement+RandomAccess.h"
#import "SCNGeometrySource+RandomAccess.h"
#import "VertexData.hpp"
#import "SharedTypes.h"

using namespace std;
using namespace simd;
using namespace z::metal;

@interface ColoredTextMesh ()

@property (nonatomic) float width;
@property (nonatomic) float height;

@property (nonatomic) NSUInteger size;

@end

@implementation ColoredTextMesh

- (instancetype)initWithDevice:(id<MTLDevice>)device
                      geometry:(SCNGeometry *)geometry
{
    self = [super init];
    if (self) {
        _argumentIndexPosition = ArgIndexPosition;
        _argumentIndexNormal = ArgIndexNormal;
        _argumentIndexColor = ArgIndexColor;
        _argumentIndexTexCoord = ArgIndexTexCoord;
        _vertexBufferIndex = BufferIndexVertex;

        NSArray<SCNGeometrySource *> *vertexSources = [geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticVertex];
        NSArray<SCNGeometrySource *> *normalSources = [geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticNormal];
        NSArray<SCNGeometrySource *> *textureSources = [geometry geometrySourcesForSemantic:SCNGeometrySourceSemanticTexcoord];

        if (vertexSources.count == 1 && normalSources.count == 1 && textureSources.count == 1) {
            SCNGeometrySource *vertexSource = [vertexSources firstObject];
            SCNGeometrySource *normalSource = [normalSources firstObject];
            SCNGeometrySource *textureSource = [textureSources firstObject];

            VertexBuffer<VertexData, uint16_t> vbuffer;
            NSMutableArray<IndexBuffer *> *indexBuffers = [NSMutableArray array];
            for (SCNGeometryElement *element in geometry.geometryElements) {
                const NSUInteger vertexCount = 3; // assume triangles
                if (element.bytesPerIndex == 2) {
                    float4 current_color = random_color();
                    for (size_t i = 0; i < element.primitiveCount * vertexCount; ++i) {
                        if (i != 0 && i % vertexCount == 0) {
                            current_color = random_color();
                        }
                        uint16_t index1 = [element indexAt:i];
                        float3 position = [vertexSource float3At:index1];
                        float3 normal = [normalSource float3At:index1];
                        float2 text_coords = [textureSource float2At:index1];
                        VertexData v{position, normal, text_coords, current_color};
                        vbuffer.push_back(v);
                    }

                    auto indices = vbuffer.indices();
                    id<MTLBuffer> metalIndexBuffer = bufferFromVector(device, indices);
                    IndexBuffer *indexBuffer = [IndexBuffer
                                                indexBuffer:metalIndexBuffer
                                                elementCount:indices.size()
                                                primiveType:MTLPrimitiveTypeTriangle
                                                indexType:MTLIndexTypeUInt16];
                    [indexBuffers addObject:indexBuffer];
                    vbuffer.reset_indices();
                } else {
                    NSAssert(false, @"implement double size indices");
                }
            }

            auto bounds = vertex_data_bounds(vbuffer.vertices());
            _width = bounds.second.x - bounds.first.x;
            _height = bounds.second.y - bounds.first.y;

            _vertexBuffer = bufferFromVector(device, vbuffer.vertices());
            self.size = vbuffer.num_vertices();

            _indexBuffers = [NSArray arrayWithArray:indexBuffers];

            size_t offset_position = offsetof(VertexData, position);
            size_t offset_normal = offsetof(VertexData, normal);
            size_t offset_tex_coords = offsetof(VertexData, tex_coords);
            size_t offset_color = offsetof(VertexData, color);
            size_t vertex_stride = sizeof(VertexData);

            MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];

            vertexDescriptor.layouts[_vertexBufferIndex].stride = vertex_stride;

            vertexDescriptor.attributes[_argumentIndexPosition].bufferIndex = _vertexBufferIndex;
            vertexDescriptor.attributes[_argumentIndexPosition].format = MTLVertexFormatFloat3;
            vertexDescriptor.attributes[_argumentIndexPosition].offset = offset_position;

            vertexDescriptor.attributes[_argumentIndexNormal].bufferIndex = _vertexBufferIndex;
            vertexDescriptor.attributes[_argumentIndexNormal].format = MTLVertexFormatFloat3;
            vertexDescriptor.attributes[_argumentIndexNormal].offset = offset_normal;

            vertexDescriptor.attributes[_argumentIndexColor].bufferIndex = _vertexBufferIndex;
            vertexDescriptor.attributes[_argumentIndexColor].format = MTLVertexFormatFloat4;
            vertexDescriptor.attributes[_argumentIndexColor].offset = offset_color;

            vertexDescriptor.attributes[_argumentIndexTexCoord].bufferIndex = _vertexBufferIndex;
            vertexDescriptor.attributes[_argumentIndexTexCoord].format = MTLVertexFormatFloat2;
            vertexDescriptor.attributes[_argumentIndexTexCoord].offset = offset_tex_coords;

            _vertexDescriptor = vertexDescriptor;
        } else {
            NSAssert(false, @"more than one level of vertex/normal/texture sources");
        }
    }
    return self;
}

- (instancetype)initWithDevice:(id<MTLDevice>)device
                        string:(NSString *)string
                      fontSize:(CGFloat)fontSize
                extrusionDepth:(CGFloat)extrusionDepth {
    [SCNTransaction begin];
    SCNText *geometry = [SCNText textWithString:string extrusionDepth:extrusionDepth];
    UIFont *font = [UIFont systemFontOfSize:fontSize weight:1];
    geometry.font = font;
    [SCNTransaction commit];

    return [self initWithDevice:device geometry:geometry];
}
@end
