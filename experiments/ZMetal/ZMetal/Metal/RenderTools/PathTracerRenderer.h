//
//  PathTracerRenderer.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#import <Foundation/Foundation.h>

#import "Rendering.h"

NS_ASSUME_NONNULL_BEGIN

@interface PathTracerRenderer : NSObject <Rendering>

@end

NS_ASSUME_NONNULL_END
