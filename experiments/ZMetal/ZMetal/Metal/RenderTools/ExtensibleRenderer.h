//
//  ExtensibleRenderer.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 3/4/24.
//

#import <Foundation/Foundation.h>
#import <MetalKit/MetalKit.h>

#import "Rendering.h"

NS_ASSUME_NONNULL_BEGIN

@interface ExtensibleRenderer : NSObject <MTKViewDelegate>

- (instancetype)initVisibleRenderer:(id<Rendering>)visibleRenderer;

@end

NS_ASSUME_NONNULL_END
