//
//  IndexBuffer.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 4/4/24.
//

#import "IndexBuffer.h"

@implementation IndexBuffer

+ (instancetype)indexBuffer:(id<MTLBuffer>)buffer
               elementCount:(NSUInteger)elementCount
                primiveType:(MTLPrimitiveType)primitiveType
                  indexType:(MTLIndexType)indexType {
    return [[IndexBuffer alloc]
            initBuffer:buffer
            elementCount:elementCount
            primiveType:primitiveType
            indexType:indexType];
}

- (instancetype)initBuffer:(id<MTLBuffer>)buffer
              elementCount:(NSUInteger)elementCount
               primiveType:(MTLPrimitiveType)primitiveType
                 indexType:(MTLIndexType)indexType {
    self = [super init];
    if (self) {
        _buffer = buffer;
        _elementCount = elementCount;
        _primitiveType = primitiveType;
        _indexType = indexType;
    }
    return self;
}

@end
