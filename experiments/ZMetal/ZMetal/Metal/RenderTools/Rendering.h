//
//  Rendering.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Rendering <NSObject>

- (nullable MTLRenderPipelineDescriptor *)renderPipelineDescriptorDevice:(id<MTLDevice>)device
                                                                    view:(nonnull MTKView *)view;

- (BOOL)createBuffersDevice:(id<MTLDevice>)device
                       view:(nonnull MTKView *)view
               commandQueue:(id<MTLCommandQueue>)commandQueue;

- (void)computeFrameDevice:(id<MTLDevice>)device
                      view:(nonnull MTKView *)view
              commandQueue:(id<MTLCommandQueue>)commandQueue;

- (void)drawView:(nonnull MTKView *)view
         encoder:(id<MTLRenderCommandEncoder>)renderCommandEncoder;

@end

NS_ASSUME_NONNULL_END
