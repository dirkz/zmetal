//
//  PathTracerRenderer.mm
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#import <Metal/Metal.h>
#import <array>
#import <vector>

#import "PathTracerRenderer.h"

#import "SharedTypes.h"
#import "TexturedVertex.hpp"
#import "ColoredVertex.hpp"
#import "MetalBuffersHelpers.h"

using namespace z::metal;
using namespace simd;

constexpr size_t quad_num_vertices = 4;
constexpr size_t quad_num_indices = 6;

@interface PathTracerRenderer ()

@end

@implementation PathTracerRenderer {
    // Scene data
    NSUInteger _triangleCount;
    id<MTLBuffer> _triangleVertexBuffer;
    id<MTLBuffer> _triangleIndexBuffer;

    // Acceleration structure on the scene data
    id<MTLAccelerationStructure> _accelerationStructure;

    // Path tracer compute pipeline that writes to a texture
    id<MTLComputePipelineState> _computePipelineState;
    MTLSize _threadgroupSize;
    MTLSize _threadgroupCount;
    id<MTLTexture> _texture;
    id<MTLBuffer> _cameraBuffer;

    // Quad display that uses the path traced texture
    id<MTLBuffer> _quadVertexBuffer;
    id<MTLBuffer> _quadIndexBuffer;
}

#pragma mark VisibleRenderer

- (nullable MTLRenderPipelineDescriptor *)renderPipelineDescriptorDevice:(id<MTLDevice>)device
                                                                    view:(nonnull MTKView *)view {
    id<MTLLibrary> library = [device newDefaultLibrary];
    if (!library) {
        NSLog(@"could not create the default library");
        return nil;
    }

    id<MTLFunction> kernelFunction = [library newFunctionWithName:@"ray_compute"];
    if (!kernelFunction) {
        NSLog(@"no compute function");
        return nil;
    }

    NSError *error = nil;
    _computePipelineState = [device newComputePipelineStateWithFunction:kernelFunction
                                                                  error:&error];
    if (!_computePipelineState) {
        NSLog(@"no compute pipeline state: %@", error);
        return nil;
    }

    id<MTLFunction> vertexFunction = [library newFunctionWithName:@"ray_vertex"];
    if (!vertexFunction) {
        NSLog(@"could not create the vertex function");
        return nil;
    }

    id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"ray_fragment"];
    if (!fragmentFunction) {
        NSLog(@"could not create the fragment function");
        return nil;
    }

    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];

    vertexDescriptor.layouts[BufferIndexVertex].stride = sizeof(TexturedVertex);

    vertexDescriptor.attributes[ArgIndexPosition].bufferIndex = BufferIndexVertex;
    vertexDescriptor.attributes[ArgIndexPosition].format = MTLVertexFormatFloat3;
    vertexDescriptor.attributes[ArgIndexPosition].offset = offsetof(TexturedVertex, position);

    vertexDescriptor.attributes[ArgIndexTexCoord].bufferIndex = BufferIndexVertex;
    vertexDescriptor.attributes[ArgIndexTexCoord].format = MTLVertexFormatFloat2;
    vertexDescriptor.attributes[ArgIndexTexCoord].offset = offsetof(TexturedVertex, tex_coords);

    MTLRenderPipelineDescriptor *pipelineDescriptor = [MTLRenderPipelineDescriptor new];
    pipelineDescriptor.vertexFunction = vertexFunction;
    pipelineDescriptor.fragmentFunction = fragmentFunction;
    pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipelineDescriptor.vertexDescriptor = vertexDescriptor;
    pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat;

    return pipelineDescriptor;
}

- (BOOL)createBuffersDevice:(id<MTLDevice>)device
                       view:(nonnull MTKView *)view
               commandQueue:(id<MTLCommandQueue>)commandQueue {
    [self createCameraBufferForDevice:device view:view];
    [self createTriangleBuffersForDevice:device view:view];
    [self createQuadBuffersForDevice:device view:view];
    [self createQuadTextureForDevice:device view:view];
    [self createAccelerationStructuresDevice:device commandQueue:commandQueue];
    return YES;
}

- (void)computeFrameDevice:(id<MTLDevice>)device
                      view:(nonnull MTKView *)view
              commandQueue:(id<MTLCommandQueue>)commandQueue {
    if (_accelerationStructure) {
        id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];

        id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];

        [computeEncoder setComputePipelineState:_computePipelineState];

        [computeEncoder setBuffer:_cameraBuffer offset:0 atIndex:BufferIndexCamera];

        [computeEncoder setAccelerationStructure:_accelerationStructure
                                   atBufferIndex:BufferIndexPrimitiveAccelerationStructure];

        [computeEncoder setTexture:_texture
                           atIndex:TextureIndexQuad];

        [computeEncoder dispatchThreadgroups:_threadgroupCount
                       threadsPerThreadgroup:_threadgroupSize];

        [computeEncoder endEncoding];

        [commandBuffer commit];
    } else {
        NSLog(@"no accelerationStructure yet");
    }
}

- (void)drawView:(nonnull MTKView *)view
         encoder:(id<MTLRenderCommandEncoder>)renderCommandEncoder {
    [renderCommandEncoder setFrontFacingWinding:MTLWindingClockwise];
    [renderCommandEncoder setCullMode:MTLCullModeBack];

    [renderCommandEncoder setFragmentTexture:_texture
                                     atIndex:TextureIndexQuad];

    [renderCommandEncoder setVertexBuffer:_quadVertexBuffer
                                   offset:0
                                  atIndex:BufferIndexVertex];

    [renderCommandEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                                     indexCount:quad_num_indices
                                      indexType:MTLIndexTypeUInt16
                                    indexBuffer:_quadIndexBuffer
                              indexBufferOffset:0];
}

#pragma mark Util

- (void)createAccelerationStructuresDevice:(id<MTLDevice>)device
                             commandQueue:(id<MTLCommandQueue>)commandQueue {
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];

    MTLAccelerationStructureTriangleGeometryDescriptor *triangles =
    [MTLAccelerationStructureTriangleGeometryDescriptor descriptor];

    triangles.triangleCount = _triangleCount;

    triangles.indexBuffer = _triangleIndexBuffer;
    triangles.indexType = MTLIndexTypeUInt16;
    triangles.indexBufferOffset = 0;

    triangles.vertexBuffer = _triangleVertexBuffer;
    triangles.vertexFormat = MTLAttributeFormatFloat3;
    triangles.vertexBufferOffset = offsetof(ColoredVertex, position);
    triangles.vertexStride = sizeof(ColoredVertex);

    MTLPrimitiveAccelerationStructureDescriptor *primitive =
    [MTLPrimitiveAccelerationStructureDescriptor descriptor];

    primitive.geometryDescriptors = @[triangles];

    id<MTLAccelerationStructure> accelerationStructure = [device
                                                          newAccelerationStructureWithDescriptor:primitive];

    MTLAccelerationStructureSizes sizes = [device
                                           accelerationStructureSizesWithDescriptor:primitive];

    id<MTLBuffer> scratchBuffer = [device
                                   newBufferWithLength:sizes.buildScratchBufferSize
                                   options:MTLStorageModeShared];

    id<MTLAccelerationStructureCommandEncoder> accCommandEncoder =
    [commandBuffer accelerationStructureCommandEncoder];

    [accCommandEncoder buildAccelerationStructure:accelerationStructure
                                       descriptor:primitive
                                    scratchBuffer:scratchBuffer
                              scratchBufferOffset:0];

    [accCommandEncoder endEncoding];

    _accelerationStructure = accelerationStructure;

    [commandBuffer commit];
}

- (void)createCameraBufferForDevice:(id<MTLDevice>)device
                               view:(nonnull MTKView *)view {
    float3 origin{0, 0, 5};
    float3 target{0, 0, 1};
    float viewAspect = view.drawableSize.width / view.drawableSize.height;
    float vpWidth = 20;
    float vpHeight = vpWidth / viewAspect;
    float2 viewport{vpWidth, vpHeight};
    float3 up{0, 1, 0};
    Camera camera{origin, target, up, viewport};
    _cameraBuffer = [device newBufferWithBytes:&camera
                                        length:sizeof(Camera)
                                       options:MTLStorageModeShared];
}

- (void)createTriangleBuffersForDevice:(id<MTLDevice>)device
                                  view:(nonnull MTKView *)view {
    float4 color{1, 0, 0, 1};

    ColoredVertex top{float3{0, 5, 0}, color};
    ColoredVertex bl{float3{-5, -5, 0}, color};
    ColoredVertex br{float3{5, -5, 0}, color};

    std::vector<ColoredVertex> vertices{top, br, bl};
    std::vector<uint16_t> indices{0, 1, 2};

    _triangleVertexBuffer = bufferFromVector(device, vertices);
    _triangleIndexBuffer = bufferFromVector(device, indices);
    _triangleCount = indices.size() / 3;
}

- (void)createQuadBuffersForDevice:(id<MTLDevice>)device
                              view:(nonnull MTKView *)view {
    float3 v_tl{-1, 1, 0};
    float3 v_tr{1, 1, 0};
    float3 v_bl{-1, -1, 0};
    float3 v_br{1, -1, 0};

    float2 t_tl{0, 0};
    float2 t_tr{1, 0};
    float2 t_bl{0, 1};
    float2 t_br{1, 1};

    std::array<TexturedVertex, quad_num_vertices> vertices{
        TexturedVertex{v_tl, t_tl},
        TexturedVertex{v_tr, t_tr},
        TexturedVertex{v_bl, t_bl},
        TexturedVertex{v_br, t_br},
    };

    std::array<uint16_t, quad_num_indices> indices{0, 1, 2, 1, 3, 2};

    _quadVertexBuffer = [device newBufferWithBytes:vertices.data()
                                            length:vertices.size() * sizeof(TexturedVertex)
                                           options:MTLStorageModeShared];

    _quadIndexBuffer = [device newBufferWithBytes:indices.data()
                                           length:indices.size() * sizeof(uint16_t)
                                          options:MTLStorageModeShared];
}

- (void)createQuadTextureForDevice:(id<MTLDevice>)device
                              view:(nonnull MTKView *)view {
    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
    textureDescriptor.textureType = MTLTextureType2D;
    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
    textureDescriptor.width = view.drawableSize.width;
    textureDescriptor.height = view.drawableSize.height;
    textureDescriptor.usage = MTLTextureUsageShaderWrite | MTLTextureUsageShaderRead;
    textureDescriptor.resourceOptions = MTLStorageModeShared;

    _texture = [device newTextureWithDescriptor:textureDescriptor];

    _threadgroupSize = MTLSizeMake(16, 16, 1);

    _threadgroupCount.width = (_texture.width  + _threadgroupSize.width -  1) / _threadgroupSize.width;
    _threadgroupCount.height = (_texture.height + _threadgroupSize.height - 1) / _threadgroupSize.height;
    _threadgroupCount.depth = 1;
}

@end
