//
//  IndexBuffer.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 4/4/24.
//

#import <Foundation/Foundation.h>
#import <Metal/Metal.h>

NS_ASSUME_NONNULL_BEGIN

/// Wraps an `MTLBuffer` with indices, also providing the number of elements and other meta information.
@interface IndexBuffer : NSObject

@property (nonatomic, readonly) id<MTLBuffer> buffer;
@property (nonatomic) NSUInteger elementCount;
@property (nonatomic) MTLPrimitiveType primitiveType;
@property (nonatomic) MTLIndexType indexType;

+ (instancetype)indexBuffer:(id<MTLBuffer>)buffer
               elementCount:(NSUInteger)elementCount
                primiveType:(MTLPrimitiveType)primitiveType
                  indexType:(MTLIndexType)indexType;

- (instancetype)initBuffer:(id<MTLBuffer>)buffer
              elementCount:(NSUInteger)elementCount
               primiveType:(MTLPrimitiveType)primitiveType
                 indexType:(MTLIndexType)indexType;

@end

NS_ASSUME_NONNULL_END
