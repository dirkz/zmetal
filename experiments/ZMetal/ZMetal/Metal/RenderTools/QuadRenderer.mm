//
//  QuadRenderer.mm
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#import <Metal/Metal.h>

#import "QuadRenderer.h"

#import "SharedTypes.h"
#import "TexturedVertex.hpp"
#import <array>

using namespace z::metal;
using namespace simd;

constexpr size_t num_vertices = 4;
constexpr size_t num_indices = 6;

@interface QuadRenderer ()

@property (nonatomic) id<MTLBuffer> vertexBuffer;
@property (nonatomic) id<MTLBuffer> indexBuffer;
@property (nonatomic) id<MTLComputePipelineState> computePipelineState;
@property (nonatomic) MTLSize threadgroupSize;
@property (nonatomic) MTLSize threadgroupCount;
@property (nonatomic) id<MTLTexture> texture;

@end

@implementation QuadRenderer

- (nullable MTLRenderPipelineDescriptor *)renderPipelineDescriptorDevice:(id<MTLDevice>)device
                                                                       view:(nonnull MTKView *)view {
    id<MTLLibrary> library = [device newDefaultLibrary];
    if (!library) {
        NSLog(@"could not create the default library");
        return nil;
    }

    id<MTLFunction> kernelFunction = [library newFunctionWithName:@"quad_compute"];
    if (!kernelFunction) {
        NSLog(@"no compute function");
        return nil;
    }
    
    NSError *error = nil;
    self.computePipelineState = [device newComputePipelineStateWithFunction:kernelFunction
                                                                      error:&error];
    if (!self.computePipelineState) {
        NSLog(@"no compute pipeline state: %@", error);
        return nil;
    }

    id<MTLFunction> vertexFunction = [library newFunctionWithName:@"quad_vertex"];
    if (!vertexFunction) {
        NSLog(@"could not create the vertex function");
        return nil;
    }

    id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"quad_fragment"];
    if (!fragmentFunction) {
        NSLog(@"could not create the fragment function");
        return nil;
    }

    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];

    vertexDescriptor.layouts[BufferIndexVertex].stride = sizeof(TexturedVertex);

    vertexDescriptor.attributes[ArgIndexPosition].bufferIndex = BufferIndexVertex;
    vertexDescriptor.attributes[ArgIndexPosition].format = MTLVertexFormatFloat3;
    vertexDescriptor.attributes[ArgIndexPosition].offset = offsetof(TexturedVertex, position);

    vertexDescriptor.attributes[ArgIndexTexCoord].bufferIndex = BufferIndexVertex;
    vertexDescriptor.attributes[ArgIndexTexCoord].format = MTLVertexFormatFloat2;
    vertexDescriptor.attributes[ArgIndexTexCoord].offset = offsetof(TexturedVertex, tex_coords);

    MTLRenderPipelineDescriptor *pipelineDescriptor = [MTLRenderPipelineDescriptor new];
    pipelineDescriptor.vertexFunction = vertexFunction;
    pipelineDescriptor.fragmentFunction = fragmentFunction;
    pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipelineDescriptor.vertexDescriptor = vertexDescriptor;
    pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat;

    return pipelineDescriptor;
}

- (BOOL)createBuffersDevice:(id<MTLDevice>)device
                       view:(nonnull MTKView *)view
               commandQueue:(id<MTLCommandQueue>)commandQueue {
    float3 v_tl{-1, 1, 0};
    float3 v_tr{1, 1, 0};
    float3 v_bl{-1, -1, 0};
    float3 v_br{1, -1, 0};

    float2 t_tl{0, 0};
    float2 t_tr{1, 0};
    float2 t_bl{0, 1};
    float2 t_br{1, 1};

    std::array<TexturedVertex, num_vertices> vertices{
        TexturedVertex{v_tl, t_tl},
        TexturedVertex{v_tr, t_tr},
        TexturedVertex{v_bl, t_bl},
        TexturedVertex{v_br, t_br},
    };

    std::array<uint16_t, num_indices> indices{0, 1, 2, 1, 3, 2};

    self.vertexBuffer = [device newBufferWithBytes:vertices.data()
                                            length:vertices.size() * sizeof(TexturedVertex)
                                           options:MTLStorageModeShared];

    self.indexBuffer = [device newBufferWithBytes:indices.data()
                                           length:indices.size() * sizeof(uint16_t)
                                          options:MTLStorageModeShared];

    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
    textureDescriptor.textureType = MTLTextureType2D;
    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
    textureDescriptor.width = view.drawableSize.width;
    textureDescriptor.height = view.drawableSize.height;
    textureDescriptor.usage = MTLTextureUsageShaderWrite | MTLTextureUsageShaderRead ;

    self.texture = [device newTextureWithDescriptor:textureDescriptor];

    self.threadgroupSize = MTLSizeMake(16, 16, 1);

    _threadgroupCount.width = (self.texture.width  + self.threadgroupSize.width -  1) / self.threadgroupSize.width;
    _threadgroupCount.height = (self.texture.height + self.threadgroupSize.height - 1) / self.threadgroupSize.height;
    _threadgroupCount.depth = 1;

    return YES;
}

- (void)computeFrameDevice:(id<MTLDevice>)device
                      view:(nonnull MTKView *)view
              commandQueue:(id<MTLCommandQueue>)commandQueue {
    id<MTLCommandBuffer> commandBuffer = [commandQueue commandBuffer];

    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];

    [computeEncoder setComputePipelineState:self.computePipelineState];

    [computeEncoder setTexture:self.texture
                       atIndex:TextureIndexQuad];

    [computeEncoder dispatchThreadgroups:self.threadgroupCount
                   threadsPerThreadgroup:self.threadgroupSize];

    [computeEncoder endEncoding];

    [commandBuffer commit];
}

- (void)drawView:(nonnull MTKView *)view
         encoder:(id<MTLRenderCommandEncoder>)renderCommandEncoder {
    [renderCommandEncoder setFrontFacingWinding:MTLWindingClockwise];
    [renderCommandEncoder setCullMode:MTLCullModeBack];

    [renderCommandEncoder setFragmentTexture:self.texture
                                     atIndex:TextureIndexQuad];

    [renderCommandEncoder setVertexBuffer:self.vertexBuffer
                                   offset:0
                                  atIndex:BufferIndexVertex];

    [renderCommandEncoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                                     indexCount:num_indices
                                      indexType:MTLIndexTypeUInt16
                                    indexBuffer:self.indexBuffer
                              indexBufferOffset:0];
}

@end
