//
//  ColoredTextRenderer.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 10/4/24.
//

#import "ColoredTextRenderer.h"

#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

#import "SharedTypes.h"
#import "ColoredTextMesh.h"
#import "Transform.hpp"
#import "IndexBuffer.h"

using simd::float4x4;
using simd::float3x3;
using simd::float3;

using namespace z::metal;
using namespace z::metal::transform;

@interface ColoredTextRenderer ()

@property (nonatomic) ColoredTextMesh *vertexDataMesh;

@end

@implementation ColoredTextRenderer

- (nullable MTLRenderPipelineDescriptor *)renderPipelineDescriptorDevice:(id<MTLDevice>)device
                                                                       view:(nonnull MTKView *)view {
    self.vertexDataMesh = [[ColoredTextMesh alloc]
                           initWithDevice:device
                           string:@"Hello"
                           fontSize:10
                           extrusionDepth:2];

    id<MTLLibrary> library = [device newDefaultLibrary];
    if (!library) {
        NSLog(@"could not create the default library");
        return nil;
    }

    id<MTLFunction> vertexFunction = [library newFunctionWithName:@"colored_text_vertex"];
    if (!vertexFunction) {
        NSLog(@"could not create the vertex function");
        return nil;
    }

    id<MTLFunction> fragmentFunction = [library newFunctionWithName:@"colored_text_fragment"];
    if (!fragmentFunction) {
        NSLog(@"could not create the fragment function");
        return nil;
    }

    NSAssert(self.vertexDataMesh.argumentIndexPosition == ArgIndexPosition,
             @"unexpected position argument index");
    NSAssert(self.vertexDataMesh.argumentIndexNormal == ArgIndexNormal,
             @"unexpected normal argument index");
    NSAssert(self.vertexDataMesh.argumentIndexColor == ArgIndexColor,
             @"unexpected color argument index");
    NSAssert(self.vertexDataMesh.argumentIndexTexCoord == ArgIndexTexCoord,
             @"unexpected texture coords argument index");
    NSAssert(self.vertexDataMesh.vertexBufferIndex == BufferIndexVertex,
             @"expected BUFFER_VERTEX as the one and only vertex buffer");

    MTLRenderPipelineDescriptor *pipelineDescriptor = [MTLRenderPipelineDescriptor new];
    pipelineDescriptor.vertexFunction = vertexFunction;
    pipelineDescriptor.fragmentFunction = fragmentFunction;
    pipelineDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;
    pipelineDescriptor.vertexDescriptor = self.vertexDataMesh.vertexDescriptor;
    pipelineDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat;

    return pipelineDescriptor;
}

- (BOOL)createBuffersDevice:(id<MTLDevice>)device
                       view:(nonnull MTKView *)view
               commandQueue:(id<MTLCommandQueue>)commandQueue {
    return YES;
}

- (void)computeFrameDevice:(id<MTLDevice>)device
                      view:(nonnull MTKView *)view
              commandQueue:(id<MTLCommandQueue>)commandQueue {
}

- (void)drawView:(nonnull MTKView *)view
         encoder:(id<MTLRenderCommandEncoder>)renderCommandEncoder {
    // set uniforms
    CGFloat aspect = view.drawableSize.width / view.drawableSize.height;
    float4x4 projection = perspective(M_PI / 3, aspect, 0.1, 100);
    float4x4 modelView = translate(float3{-self.vertexDataMesh.width / 2, -self.vertexDataMesh.height, -20});
    float3x3 normal = normal_transform(modelView);
    auto uniforms = Projection{modelView, projection, normal};
    [renderCommandEncoder setVertexBytes:&uniforms
                                  length:sizeof(Projection)
                                 atIndex:BufferIndexUniformProjection];

    [renderCommandEncoder setFrontFacingWinding:MTLWindingCounterClockwise];
    [renderCommandEncoder setCullMode:MTLCullModeBack];

    // set vertex buffers
    [renderCommandEncoder setVertexBuffer:self.vertexDataMesh.vertexBuffer
                                   offset:0
                                  atIndex:self.vertexDataMesh.vertexBufferIndex];

    for (IndexBuffer *indexBuffer in self.vertexDataMesh.indexBuffers) {
        [renderCommandEncoder drawIndexedPrimitives:indexBuffer.primitiveType
                                         indexCount:indexBuffer.elementCount
                                          indexType:indexBuffer.indexType
                                        indexBuffer:indexBuffer.buffer
                                  indexBufferOffset:0];
    }
}

@end
