//
//  ExtensibleRenderer.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 3/4/24.
//

#import "ExtensibleRenderer.h"

@interface ExtensibleRenderer ()

@property (nonatomic) id<Rendering> actualRenderer;

@property (nonatomic) BOOL isInitialized;

@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLDepthStencilState> depthState;
@property (nonatomic) id<MTLRenderPipelineState> pipelineState;
@property (nonatomic) id<MTLCommandQueue> commandQueue;

@end

@implementation ExtensibleRenderer {
    dispatch_queue_t _once;
}

- (instancetype)initVisibleRenderer:(id<Rendering>)visibleRenderer {
    self = [super init];
    if (self) {
        _actualRenderer = visibleRenderer;
    }
    return self;
}

- (void)drawInMTKView:(nonnull MTKView *)view {
    if (![self checkInitialized:view]) {
        return;
    }
    
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    if (!commandBuffer) {
        NSLog(@"no command buffer");
        return;
    }

    MTLRenderPassDescriptor *renderPassDescriptor = [view currentRenderPassDescriptor];
    if (!renderPassDescriptor) {
        NSLog(@"no render pass descriptor");
        return;
    }

    id<CAMetalDrawable> drawable = view.currentDrawable;
    if (!drawable) {
        NSLog(@"no current drawable");
        return;
    }

    [self.actualRenderer computeFrameDevice:self.device
                                       view:view
                               commandQueue:self.commandQueue];

    id<MTLRenderCommandEncoder> renderCommandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    if (!renderCommandEncoder) {
        NSLog(@"no render command encoder");
        return;
    }

    view.clearDepth = 1.0;

    [renderCommandEncoder setRenderPipelineState:self.pipelineState];
    [renderCommandEncoder setDepthStencilState:self.depthState];

    [self.actualRenderer drawView:view
                          encoder:renderCommandEncoder];

    [renderCommandEncoder endEncoding];

    [commandBuffer presentDrawable:drawable];
    [commandBuffer commit];
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
}

- (BOOL)checkInitialized:(nonnull MTKView *)view {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _once = dispatch_queue_create("once", DISPATCH_QUEUE_SERIAL);
        dispatch_async(_once, ^{
            BOOL isInitialized = [self initPipelines:view];
            if (isInitialized) {
                isInitialized = [self.actualRenderer
                                 createBuffersDevice:self.device
                                 view:view
                                 commandQueue:self.commandQueue];
                self.isInitialized = isInitialized;
            }
        });
    });
    return self.isInitialized;
}

- (BOOL)initPipelines:(nonnull MTKView *)view {
    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm;
    view.depthStencilPixelFormat = MTLPixelFormatDepth16Unorm;

    id<MTLDevice> device = MTLCreateSystemDefaultDevice();
    if (!device) {
        NSLog(@"could not create the default device");
        return NO;
    }
    view.device = device;
    self.device = device;

    MTLDepthStencilDescriptor *depthDescriptor = [MTLDepthStencilDescriptor new];
    depthDescriptor.depthCompareFunction = MTLCompareFunctionLessEqual;
    depthDescriptor.depthWriteEnabled = YES;
    self.depthState = [device newDepthStencilStateWithDescriptor:depthDescriptor];

    MTLRenderPipelineDescriptor *pipelineDescriptor = [self.actualRenderer
                                                       renderPipelineDescriptorDevice:device
                                                       view:view];

    NSError *error = nil;
    self.pipelineState = [device newRenderPipelineStateWithDescriptor:pipelineDescriptor error:&error];
    if (!self.pipelineState) {
        NSLog(@"error creating the render pipeline state: %@", error);
        return NO;
    }

    self.commandQueue = [device newCommandQueue];

    return YES;
}

@end
