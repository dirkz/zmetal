//
//  ViewController.m
//  ZMetal
//
//  Created by Dirk Zimmermann on 3/4/24.
//

#import <MetalKit/MetalKit.h>

#import "ViewController.h"

#import "ExtensibleRenderer.h"
#import "ColoredTextRenderer.h"
#import "QuadRenderer.h"
#import "PathTracerRenderer.h"

#import "RayTracer.h"

@interface ViewController ()

@property (nonatomic, nullable) RayTracer *renderer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    MTKView *view = (MTKView *) self.view;
    self.renderer = [[RayTracer alloc] init];
    view.delegate = self.renderer;
}


@end
