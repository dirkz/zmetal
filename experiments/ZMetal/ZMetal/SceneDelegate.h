//
//  SceneDelegate.h
//  ZMetal
//
//  Created by Dirk Zimmermann on 11/4/24.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

