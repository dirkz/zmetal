//
//  Vec3Tests.m
//  ZMetalTests
//
//  Created by Dirk Zimmermann on 11/4/24.
//

#import <XCTest/XCTest.h>
#import <simd/simd.h>

#import <array>

#import "Vec3.hpp"
#import "Tools.hpp"

using namespace simd;
using namespace z::metal;

@interface Vec3Tests : XCTestCase

@end

@implementation Vec3Tests

- (void)testDefaultConstructor {
    Vec3<int> v{};
    XCTAssertEqual(v.x(), 0);
    XCTAssertEqual(v.y(), 0);
    XCTAssertEqual(v.z(), 0);
}

- (void)testExplicitConstructor {
    Vec3<int> v{1, 2, 3};
    XCTAssertEqual(v.x(), 1);
    XCTAssertEqual(v.y(), 2);
    XCTAssertEqual(v.z(), 3);
}

template<class T>
Vec3<T> randomVec() {
    return Vec3<T>{random<T>(), random<T>(), random<T>()};
}

double3 randomSimdDouble() {
    return double3{random_double(), random_double(), random_double()};
}

float3 randomSimdFloat() {
    return float3{random_float(), random_float(), random_float()};
}

constexpr size_t num_vectors = 1000;

- (void)testAdditionPerformanceVecDouble {
    std::array<Vec3<double>, num_vectors> vs1{};
    std::array<Vec3<double>, num_vectors> vs2{};

    for (size_t i = 0; i < num_vectors; ++i) {
        vs1[i] = randomVec<double>();
        vs2[i] = randomVec<double>();
    }

    [self measureBlock:^{
        for (size_t i = 0; i < num_vectors; ++i) {
            Vec3<double> v1 = vs1[i] + vs2[i];
            Vec3<double> v2 = vs1[i] * vs2[i];
            v1 = v1 - v2;
        }
    }];
}

- (void)testAdditionPerformanceSimdDouble {
    std::array<double3, num_vectors> vs1{};
    std::array<double3, num_vectors> vs2{};

    for (size_t i = 0; i < num_vectors; ++i) {
        vs1[i] = randomSimdDouble();
        vs2[i] = randomSimdDouble();
    }

    [self measureBlock:^{
        for (size_t i = 0; i < num_vectors; ++i) {
            double3 v1 = vs1[i] + vs2[i];
            double3 v2 = vs1[i] * vs2[i];
            v1 = v1 - v2;
        }
    }];
}

- (void)testAdditionPerformanceVecFloat {
    std::array<Vec3<float>, num_vectors> vs1{};
    std::array<Vec3<float>, num_vectors> vs2{};

    for (size_t i = 0; i < num_vectors; ++i) {
        vs1[i] = randomVec<float>();
        vs2[i] = randomVec<float>();
    }

    [self measureBlock:^{
        for (size_t i = 0; i < num_vectors; ++i) {
            Vec3<float> v1 = vs1[i] + vs2[i];
            Vec3<float> v2 = vs1[i] * vs2[i];
            v1 = v1 - v2;
        }
    }];
}

- (void)testAdditionPerformanceSimdFloat {
    std::array<float3, num_vectors> vs1{};
    std::array<float3, num_vectors> vs2{};

    for (size_t i = 0; i < num_vectors; ++i) {
        vs1[i] = randomSimdFloat();
        vs2[i] = randomSimdFloat();
    }

    [self measureBlock:^{
        for (size_t i = 0; i < num_vectors; ++i) {
            float3 v1 = vs1[i] + vs2[i];
            float3 v2 = vs1[i] * vs2[i];
            v1 = v1 - v2;
        }
    }];
}

@end
