//
//  PerlinNoiseTests.m
//  ZMetalTests
//
//  Created by Dirk Zimmermann on 6/4/24.
//

#import <XCTest/XCTest.h>

#import "Perlin.h"

@interface PerlinNoiseTests : XCTestCase

@end

@implementation PerlinNoiseTests

- (void)testFmod {
    double d = 5.0;
    XCTAssertEqual(fmod(6.0, d), 1.0);
    XCTAssertEqual(fmod(4.0, d), 4.0);
    XCTAssertEqual(fmod(1.0, d), 1.0);
    XCTAssertEqual(fmod(4.99, d), 4.99);
    XCTAssertEqual(fmod(0.0, d), 0.0);

    XCTAssertEqual(fmod(5.0, d), 0.0);
    XCTAssertEqual(fmod(-5.0, d), 0.0);

    XCTAssertEqual(fmod(-6.0, d), -1.0);
    XCTAssertEqual(fmod(-4.0, d), -4.0);
    XCTAssertEqual(fmod(-1.0, d), -1.0);
    XCTAssertEqual(fmod(-4.99, d), -4.99);
}

- (void)testCeilFloor {
    XCTAssertEqual(floor(4.99), 4.0);
    XCTAssertEqual(floor(-4.99), -5.0);
    XCTAssertEqual(ceil(4.99), 5.0);
    XCTAssertEqual(ceil(-4.99), -4.0);
}

- (void)cubeCoordinates {
    using namespace z::metal::perlin;

    int2 dimensions = int2{5, 5};
    for (double x = -dimensions.x - 1; x < dimensions.x + 1; ++x) {
        for (double y = -dimensions.y - 1; y < dimensions.y + 1; ++y) {
            int2 coords = cube_coordinates(dimensions, double2{x, y});
            XCTAssert(coords.x > 0);
            XCTAssert(coords.y > 0);
            XCTAssert(coords.x < dimensions.x);
            XCTAssert(coords.y < dimensions.y);
        }
    }
}

@end
