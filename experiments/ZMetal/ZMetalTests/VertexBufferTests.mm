//
//  VertexBufferTests.m
//  ZRayTests
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <XCTest/XCTest.h>

#import <vector>

#import "VertexBuffer.hpp"

using namespace z::metal;

@interface VertexBufferTests : XCTestCase

@end

@implementation VertexBufferTests

- (void)test {
    VertexBuffer<int, int> vb{0, 1, 2, 3};
    auto expected_indices = std::vector<int>{0, 1, 2, 3};
    XCTAssertEqual(vb.indices(), expected_indices);
    XCTAssertEqual(vb.vertices(), expected_indices);
}

- (void)testDuplicates {
    VertexBuffer<int, int> vb{0, 1, 2, 1, 2};
    auto expected_indices = std::vector<int>{0, 1, 2, 1, 2};
    auto expected_values = std::vector<int>{0, 1, 2};
    XCTAssertEqual(vb.indices(), expected_indices);
    XCTAssertEqual(vb.vertices(), expected_values);
}

@end
