//
//  VertexData.cpp
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#include "VertexData.hpp"

bool operator<(const VertexData& v1, const VertexData& v2) {
    return
    v1.position.x < v1.position.x ||
    v1.position.y < v1.position.y ||
    v1.position.z < v1.position.z ||
    v1.tex_coords.x < v1.tex_coords.x ||
    v1.tex_coords.y < v1.tex_coords.y;
}
