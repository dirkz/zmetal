//
//  VertexData.hpp
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#ifndef VertexData_hpp
#define VertexData_hpp

#include <simd/simd.h>

struct VertexData {
    simd::float3 position;
    simd::float2 tex_coords;
};

bool operator<(const VertexData& v1, const VertexData& v2);

#endif /* VertexData_hpp */
