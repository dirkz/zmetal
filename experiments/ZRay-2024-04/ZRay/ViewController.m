//
//  ViewController.m
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <MetalKit/MetalKit.h>

#import "ViewController.h"
#import "Renderer.h"

@interface ViewController ()

@property (nonatomic) Renderer *renderer;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    MTKView *view = (MTKView *) self.view;
    self.renderer = [[Renderer alloc] initWithView:view];

}


@end
