//
//  Shaders.metal
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#include <metal_stdlib>
using namespace metal;

#include "SharedTypes.h"

kernel void
generateTexture(texture2d<float, access::write> texture [[texture(BufferTexture)]],
                uint2 gid [[thread_position_in_grid]]) {
    if (gid.x >= texture.get_width()) {
        return;
    }
    if (gid.y >= texture.get_height()) {
        return;
    }
    
    texture.write(float4(gid.x, gid.y, 1, 1), gid);
}

struct VertexData {
    float3 position [[attribute(AttrPosition)]];
    float2 tex_coords [[attribute(AttrTexture)]];
};

struct FragmentData {
    float4 position [[position]];
    float2 tex_coords;
};

vertex FragmentData vertex_main(VertexData vert [[stage_in]]) {
    FragmentData frag;
    frag.position = float4(vert.position, 1);
    frag.tex_coords = vert.tex_coords;
    return frag;
}

fragment float4 fragment_main(FragmentData frag [[stage_in]]) {
    return float4{1, 0, 0, 1};
}
