//
//  SharedTypes.h
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#ifndef SharedTypes_h
#define SharedTypes_h

typedef enum _Attr {
    AttrPosition,
    AttrTexture
} Attr;

typedef enum _Buffer {
    BufferVertex,
    BufferTexture
} Buffer;

#endif /* SharedTypes_h */
