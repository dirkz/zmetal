//
//  Renderer.h
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import <MetalKit/MetalKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface Renderer : NSObject <MTKViewDelegate>

- (instancetype)initWithView:(MTKView *)view;

@end

NS_ASSUME_NONNULL_END
