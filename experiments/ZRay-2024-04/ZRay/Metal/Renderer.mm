//
//  Renderer.m
//  ZRay
//
//  Created by Dirk Zimmermann on 9/4/24.
//

#import "Renderer.h"

#import "SharedTypes.h"
#import "VertexData.hpp"
#import "VertexBuffer.hpp"

using simd::float3;
using simd::float2;

@interface Renderer ()

@property (nonatomic) BOOL hasBeenInitialized;
@property (nonatomic) id<MTLDevice> device;
@property (nonatomic) id<MTLLibrary> defaultLibrary;
@property (nonatomic) id<MTLCommandQueue> commandQueue;
@property (nonatomic) id<MTLComputePipelineState> computePipelineState;

@property (nonatomic) id<MTLTexture> renderTexture;
@property (nonatomic) MTLSize threadgroupSize;
@property (nonatomic) MTLSize threadgroupCount;

@property (nonatomic) id<MTLDepthStencilState> depthState;
@property (nonatomic) id<MTLRenderPipelineState> renderPipelineState;

@property (nonatomic) MTLVertexDescriptor *vertexDescriptor;
@property (nonatomic) id<MTLBuffer> quadVertexBuffer;
@property (nonatomic) id<MTLBuffer> quadIndexBuffer;
@property (nonatomic) NSUInteger numIndices;

@end

@implementation Renderer {
    dispatch_queue_t _queue;
}

- (instancetype)initWithView:(MTKView *)view
{
    self = [super init];
    if (self) {
        view.delegate = self;
    }
    return self;
}

- (void)drawInMTKView:(nonnull MTKView *)view {
    if (!self.hasBeenInitialized) {
        NSLog(@"not yet fully initialized");
        return;
    }

    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    if (!commandBuffer) {
        NSLog(@"no command buffer");
        return;
    }

    MTLRenderPassDescriptor *renderPassDescriptor = [view currentRenderPassDescriptor];
    if (!renderPassDescriptor) {
        NSLog(@"no render pass descriptor");
        return;
    }

    id<CAMetalDrawable> drawable = view.currentDrawable;
    if (!drawable) {
        NSLog(@"no current drawable");
        return;
    }

    id<MTLRenderCommandEncoder> encoder = [commandBuffer renderCommandEncoderWithDescriptor:renderPassDescriptor];
    if (!encoder) {
        NSLog(@"no render command encoder");
        return;
    }

    view.clearDepth = 1.0;

    [encoder setRenderPipelineState:self.renderPipelineState];
    [encoder setCullMode:MTLCullModeFront];
    [encoder setTriangleFillMode:MTLTriangleFillModeFill];
    [encoder setDepthStencilState:self.depthState];

    // set vertex buffers
    [encoder setVertexBuffer:self.quadVertexBuffer
                      offset:0
                     atIndex:BufferVertex];

    [encoder drawIndexedPrimitives:MTLPrimitiveTypeTriangle
                        indexCount:self.numIndices
                         indexType:MTLIndexTypeUInt16
                       indexBuffer:self.quadIndexBuffer
                 indexBufferOffset:0];

    [encoder endEncoding];

    [commandBuffer presentDrawable:drawable];
    [commandBuffer commit];
}

- (void)mtkView:(nonnull MTKView *)view drawableSizeWillChange:(CGSize)size {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _queue = dispatch_queue_create("dispatch_once init queue", DISPATCH_QUEUE_SERIAL);
        dispatch_async(_queue, ^{
            [self initOnceView:view size:size];
            [self createVertexData];
            [self createRenderPipelineStateView:view];
            [self recomputeTextureSize:size];
            self.hasBeenInitialized = YES;
        });
    });

    if (self.hasBeenInitialized) {
        [self recomputeTextureSize:size];
    }
}

- (void)createRenderPipelineStateView:(nonnull MTKView *)view {
    MTLDepthStencilDescriptor *depthDescriptor = [MTLDepthStencilDescriptor new];
    depthDescriptor.depthCompareFunction = MTLCompareFunctionLessEqual;
    depthDescriptor.depthWriteEnabled = YES;
    self.depthState = [self.device newDepthStencilStateWithDescriptor:depthDescriptor];
    NSAssert(self.depthState, @"no depth state");

    id<MTLFunction> vertexFunction = [self.defaultLibrary newFunctionWithName:@"vertex_main"];
    NSAssert(vertexFunction, @"no vertex function");
    id<MTLFunction> fragmentFunction = [self.defaultLibrary newFunctionWithName:@"fragment_main"];
    NSAssert(fragmentFunction, @"no fragment function");

    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
    pipelineStateDescriptor.label = @"Simple Render Pipeline";
    pipelineStateDescriptor.vertexFunction = vertexFunction;
    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
    pipelineStateDescriptor.vertexDescriptor = self.vertexDescriptor;
    pipelineStateDescriptor.depthAttachmentPixelFormat = view.depthStencilPixelFormat;
    pipelineStateDescriptor.colorAttachments[0].pixelFormat = view.colorPixelFormat;

    NSError *error = nil;
    self.renderPipelineState = [self.device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor
                                                                           error:&error];
    NSAssert(self.renderPipelineState, @"no render pipeline: %@", error);
}

- (void)createVertexData {
    VertexData bl{float3{-1, -1 , 0}, float2{0, 1}};
    VertexData br{float3{1, -1 , 0}, float2{1, 1}};
    VertexData tl{float3{-1, 1 , 0}, float2{0, 0}};
    VertexData tr{float3{1, 1 , 0}, float2{1, 0}};

    VertexBuffer<VertexData, uint16_t> vb{tl, bl, br, tl, br, tr};

    self.quadVertexBuffer = [self.device newBufferWithBytes:vb.vertices().data()
                                                     length:vb.vertex_size()
                                                    options:MTLResourceStorageModeShared];
    self.quadIndexBuffer = [self.device newBufferWithBytes:vb.indices().data()
                                                    length:vb.index_size()
                                                   options:MTLResourceStorageModeShared];

    self.numIndices = vb.num_indices();

    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];

    vertexDescriptor.layouts[BufferVertex].stride = sizeof(VertexData);

    vertexDescriptor.attributes[AttrPosition].bufferIndex = BufferVertex;
    vertexDescriptor.attributes[AttrPosition].format = MTLVertexFormatFloat3;
    vertexDescriptor.attributes[AttrPosition].offset = offsetof(VertexData, position);

    vertexDescriptor.attributes[AttrTexture].bufferIndex = BufferVertex;
    vertexDescriptor.attributes[AttrTexture].format = MTLVertexFormatFloat2;
    vertexDescriptor.attributes[AttrTexture].offset = offsetof(VertexData, tex_coords);

    self.vertexDescriptor = vertexDescriptor;
}

- (void)recomputeTextureSize:(CGSize)size {
    id<MTLCommandBuffer> commandBuffer = [self.commandQueue commandBuffer];
    NSAssert(commandBuffer, @"no command buffer");

    id<MTLComputeCommandEncoder> computeEncoder = [commandBuffer computeCommandEncoder];

    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
    textureDescriptor.textureType = MTLTextureType2D;
    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
    textureDescriptor.width = size.width;
    textureDescriptor.height = size.height;
    textureDescriptor.usage = MTLTextureUsageShaderRead | MTLTextureUsageShaderWrite;

    self.renderTexture = [_device newTextureWithDescriptor:textureDescriptor];

    self.threadgroupSize = MTLSizeMake(16, 16, 1);

    _threadgroupCount.width = (self.renderTexture.width + self.threadgroupSize.width - 1) / self.threadgroupSize.width;
    _threadgroupCount.height = (self.renderTexture.height + self.threadgroupSize.height - 1) / _threadgroupSize.height;
    _threadgroupCount.depth = 1;

    [computeEncoder setComputePipelineState:self.computePipelineState];

    [computeEncoder setTexture:self.renderTexture
                       atIndex:BufferTexture];

    [computeEncoder dispatchThreadgroups:self.threadgroupCount
                   threadsPerThreadgroup:self.threadgroupSize];

    [computeEncoder endEncoding];
    [commandBuffer commit];
}

- (void)initOnceView:(nonnull MTKView *)view size:(CGSize)size {
    view.colorPixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
    view.depthStencilPixelFormat = MTLPixelFormatDepth16Unorm;

    self.device = MTLCreateSystemDefaultDevice();
    NSAssert(self.device, @"no device");

    view.device = self.device;

    id<MTLLibrary> defaultLibrary = [self.device newDefaultLibrary];
    NSAssert(defaultLibrary, @"no default library");
    self.defaultLibrary = defaultLibrary;

    id<MTLFunction> kernelFunction = [defaultLibrary newFunctionWithName:@"generateTexture"];

    NSError *error = nil;
    self.computePipelineState = [self.device
                                 newComputePipelineStateWithFunction:kernelFunction
                                 error:&error];

    self.commandQueue = [self.device newCommandQueue];
    NSAssert(self.commandQueue, @"no command queue");
}

@end
